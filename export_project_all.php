<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.8.0, 2014-03-02
 */

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Asia/Taipei');


if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');
require_once 'libs/db.class.php';
require_once 'config.php';

/** Include PHPExcel */
require_once dirname(__FILE__) . '/Classes/PHPExcel.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("IBK")
							 ->setLastModifiedBy("IBK")
							 ->setTitle("PHPExcel Test Document")
							 ->setSubject("PHPExcel Test Document")
							 ->setDescription("Test document for PHPExcel, generated using PHP classes.")
							 ->setKeywords("office PHPExcel php")
							 ->setCategory("Test result file");							 

/* Performing SQL query */
$project_id = json_decode($_POST["data"], TRUE);
$db = new MyDB($DSN);

$sql = "select * from project where pro_id = {$project_id}";
$rs = $db->obj->getRow($sql);
$project_name = $rs["pro_name"];
$pro_type = $rs["pro_type"];
$pro_lv2_id = $rs["pro_lv2_id"];

if (empty($project_name)) {
    $project_name = "專案";
}
//公版
if ($pro_type == 1) {
    $sql = "select voc_id, voc_name as tab_name from vocabulary where voc_pid = {$pro_lv2_id} and voc_level = 3 order by voc_id";

    $pro_ids = $db->obj->getAll($sql);

    foreach ($pro_ids as $k => $v) {
        $sql = "select voc_id, voc_name from vocabulary where voc_pid = {$v['voc_id']} and voc_level = 4 order by voc_id";
        $rs = $db->obj->getAll($sql);
        $ids = [];
        foreach ($rs as $key => $value) {
            $ids[] = $value['voc_id'];
        }
        $v['data'] = implode(',', $ids);
        $pro_ids[$k] = $v;
    }

} else {
    $sql = "select c.*, d.tag_name as tab_name from
(select b.tag_id from project a left join tag b on a.pro_id = b.tag_pro_id where a.pro_id = {$project_id} group by tag_id) c 
left join tag d on c.tag_id = d.tag_id ";

    $pro_ids = $db->obj->getAll($sql);

    foreach ($pro_ids as $k => $v) {
        $sql = "select glo_voc_id from glossary where glo_tag_id = {$v['tag_id']}";
        $rs = $db->obj->getAll($sql);
        $ids = [];
        foreach ($rs as $key => $value) {
            $ids[] = $value['glo_voc_id'];
        }
        $v['data'] = implode(',', $ids);
        $pro_ids[$k] = $v;
    }

}
foreach ($pro_ids as $key => $value) {
    if (!empty($value["tab_name"])) {
        //$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($key, 1, $value["tab_name"]);
        $objPHPExcel->getActiveSheet()->mergeCellsByColumnAndRow($key * 2, 1, $key * 2 + 1, 1);
        $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($key * 2, 1, $value["tab_name"]);
        $ids = $value["data"];
        if ($ids) {
            $sql = "select lv4.voc_name as voc_name4, lv6.voc_name as voc_name6, lv6.voc_value from (
				(select voc_id, voc_pid, voc_level, voc_name from vocabulary where voc_level = 4 ) lv4 
				inner join (select voc_id, voc_pid, voc_level from vocabulary where voc_level = 5 ) lv5 on lv4.voc_id = lv5.voc_pid 
				inner join (select voc_id, voc_pid, voc_level, voc_name, voc_value from vocabulary where voc_level = 6 ) lv6 on lv5.voc_id = lv6.voc_pid 
				) where lv4.voc_id in ({$ids}) order by lv4.voc_id";

            $rs = $db->obj->getAll($sql);
            $i = 2;
            foreach ($rs as $k => $v) {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($key * 2, $i, html_entity_decode($v["voc_name6"]));
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($key * 2 + 1, $i, $v["voc_value"]);
                $i++;
            }
        }

        //${"arr".$key} = array();
    }
}


$highestColumn = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
$test = 'A1:'.$highestColumn.'1';
$objPHPExcel->getActiveSheet()->getStyle($test)->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
$objPHPExcel->getActiveSheet()
    ->getStyle($test)
    ->getFill()
    ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
    ->getStartColor()
    ->setARGB('FFCCCCCC');
/*
foreach ($lv6_ids as $key => $value) {
	if (!empty($value)) {
		$data = explode("_", $value);
		foreach ($lv4_ids as $k => $v) {
			if (!empty($v)) {
				if ($data[0] == $v) {
					${"arr".$k}[] = $data[1];
				}
			}
		}		
	}                     
}


foreach ($lv4_ids as $key => $value) {
	if (!empty($value)) {
		$i = 2;
		foreach (${"arr".$key} as $k => $v) {
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($key, $i, $v);
			$i++;
		}
	}                     
}
*/
$timestamp = date("YmdHis");
$filename = $project_name;
$filename .= "_[特性詞+統計詞]_" . $timestamp . ".xlsx";
//$filename = iconv('UTF-8','Big5',$filename);
// Add some data
/*
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Hello')
            ->setCellValue('B2', 'world!')
            ->setCellValue('C1', 'Hello')
            ->setCellValue('D2', 'world!');
*/
// Miscellaneous glyphs, UTF-8
/*
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A4', 'Miscellaneous glyphs')
            ->setCellValue('A5', 'éàèùâêîôûëïüÿäöüç');
*/
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle('特性詞');


// Set active sheet index to the first sheet, so Excel opens this as the first sheet

$objPHPExcel->createSheet();
$objWorkSheet = $objPHPExcel->setActiveSheetIndex(1);

// Rename sheet
$objWorkSheet->setTitle("統計詞");

//公版
if ($pro_type == 1) {
    $sql = "select * from project where pro_id = {$project_id}";
    $rs = $db->obj->getRow($sql);
    $pro_type = $rs["pro_type"];
    $pro_lv2_id = $rs["pro_lv2_id"];
    $sql = "select b.voc_id as glo_voc_id from 
(select * from vocabulary where voc_pid = {$pro_lv2_id} and voc_level = 3) a left join 
(select * from vocabulary where voc_level = 4) b on a.voc_id = b.voc_pid order by b.voc_id";

} else {
    $sql = "select glo_voc_id from project a left join tag b on a.pro_id = b.tag_pro_id left join glossary c on b.tag_id = c.glo_tag_id where glo_flag = 1 and pro_id = {$project_id}";
}
//echo $sql;exit;
$lv4 = $db->obj->getAll($sql);
$col = 0;
foreach ($lv4 as $key => $value) {
	if (!empty($value['glo_voc_id'])) {
		$id = $value['glo_voc_id'];
		$sql = "select voc_name from vocabulary where voc_id = {$id}";
		$item = $db->obj->getRow($sql);
		//${"arr".$key} = array();

		$sql = "select lv6.voc_name  from (
			(select voc_id, voc_pid, voc_level, voc_name from vocabulary where voc_level = 4 ) lv4 
			inner join (select voc_id, voc_pid, voc_level from vocabulary where voc_level = 5 ) lv5 on lv4.voc_id = lv5.voc_pid 
			inner join (select voc_id, voc_pid, voc_level, voc_name from vocabulary where voc_level = 6 ) lv6 on lv5.voc_id = lv6.voc_pid 
			) where lv4.voc_id = {$id}";
		$rs = $db->obj->getAll($sql);
		if (count($rs) > 1) {
			//$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($key, 1, $value);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, html_entity_decode($item['voc_name']));
			$i = 2;
			
			foreach ($rs as $k => $v) {
				//$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($key, $i, $v["voc_name"]);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $i, html_entity_decode($v["voc_name"]));
				$i++;
			}
			$col++;			
		}

	}                     
}
$lastColumn = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
$lastColumn++;
for ($column = 'A'; $column != $lastColumn; $column++) {
	$objPHPExcel->getActiveSheet()
	    ->getStyle($column . '1')
	    ->getFill()
	    ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
	    ->getStartColor()
	    ->setARGB('FFCCCCCC');
	$objPHPExcel->getActiveSheet()->getStyle($column . '1')->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);    	
}
$objPHPExcel->setActiveSheetIndex(0);      
ob_end_clean();
// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=utf-8');
header('Content-Disposition: attachment;filename="' . $filename . '"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
