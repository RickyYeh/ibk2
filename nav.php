 <nav class="navbar navbar-default">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#"> </a>
    </div>
    <ul class="nav navbar-nav">
      <?php
        $filename = basename($_SERVER['PHP_SELF']);
      ?>
      <li <?php if ($filename == "index.php") { ?> class="active"<?php } ?>><a href="index.php">總 覽</a></li>
      <?php 
      if ($_SESSION["usr_type"] == 1 || $_SESSION["usr_type"] == 2) {
      ?>			      
      <li <?php if ($filename == "main.php") { ?> class="active"<?php } ?>><a href="main.php">維 護</a></li>
      <?php
        }
  	  ?> 	
      <li <?php if ($filename == "export.php") { ?> class="active"<?php } ?>><a href="export.php">資料檢視</a></li>
      <?php 
      if ($_SESSION["usr_type"] == 1 || $_SESSION["usr_type"] == 2) {
      ?>				      
      <li <?php if ($filename == "import.php") { ?> class="active"<?php } ?>><a href="import.php">匯入</a></li>
      <?php
        }
  	  ?>			
  	  <li <?php if ($filename == "project.php") { ?> class="active"<?php } ?>><a href="project.php">專案設定</a></li>      
      <?php 
      if ($_SESSION["usr_type"] == 1) {
      ?>
      <li <?php if ($filename == "user.php") { ?> class="active"<?php } ?>><a href="user.php">權限管理</a></li>
      <?php
  		}
      ?>
    </ul>
    <ul class="nav navbar-nav navbar-right">
      <li><a href="#"><span class="glyphicon glyphicon-user"></span> <?php echo $_SESSION["usr_name"]?></a></li>
      <li><a href="logout.php"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
    </ul>
  </div>
</nav>	