<?php

require_once('PDO_DB.php');

class MyDB {

    var $obj = null;

    function __construct($DSN) {
        $conn = NULL;
        try {     
            $conn = new PDO_DB($DSN['db'], $DSN['user'], $DSN['pwd']);
            $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $conn->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
            //$conn->exec("SET NAMES utf8");
        } catch (PDOException $e) {
            die('Cannot connect to mysql server!');
            // 連線錯誤訊息由前台處理回覆碼
        }
        //$this->obj = ($e) ? 'ERROR' : $conn;
        $this->obj = $conn;
    }

    public function getConnection() {
        return $this->db;
    }

    function query($sql) {
        if (is_array($sql) && !empty($sql)) {
            $sqls = $sql[0];
            $params = $sql[1];
            $stmt = $this->obj->prepare($sqls);
            if (!empty($params)) {
                $this->obj->bindArray($stmt, $params);
            }
            $stmt->execute();
        } else {
            $stmt = $this->obj->query($sql);
        }
        return new Result($stmt);
    }

    function getOne($sql) {
        return $this->obj->getOne($sql);
    }

    function getRow($sql) {
        return $this->obj->getRow($sql);
    }

    function getAll($sql) {
        return $this->obj->getAll($sql);
    }

    function isError($result) {
        if (is_array($result)) {
            return true;
        } else {
            return $result->errorCode() != PDO::ERR_NONE;
        }
    }

    /**
     * 
     * @param $options mixed
     *     type: insert|select|delete|update|none
     *     columns: array|string
     *     tables: array|string 
     *     sets: array|string
     *     limit: string|false
     *     order: string
     *     sql: if type is none
     * @return sql query string
     */
    function createQueryString($options) {
        $str = array();
        $temp = array();
        $str[0] = $options['sql'] ? trim($options['sql']) : '';

        switch (strtolower($options['type'])) {

            case 'none':
                $str[0] = $options['sql'];
                break;
            case 'select':
                $str[0] = $this->createSelect($options);
                break;
            case 'update':
                $str[0] = $this->createUpdate($options);
                if (is_array($options['sets'])) {
                    $sets = $options['sets'];
                    foreach ($sets as $key => $val) {
                        $p[] = $val;
                    }
                    $temp = $p;
                } else {
                    $temp = array();
                }
                break;
            case 'insert':
                $str[0] = $this->createInsert($options);
                if (is_array($options['values'])) {
                    $values = $options['values'];
                    foreach ($options['values'] as $key => $val) {
                        if (is_null($val)) {
                            $val = trim($val);
                        }
                        $p[] = $val;
                    }
                    $temp = $p;
                } else {
                    $temp = array();
                }
                break;
            case 'delete':
                $str[0] = $this->createDelete($options);
                break;
        }

        if (strtolower($options['type']) != "insert" && is_array($options['where'])) {
            $where = $options['where'];
            foreach ($where as $key => $val) {
                $p[] = $val;
            }
            if (!empty($temp)) {
                $str[1] = $temp + $p;
            } else {
                $str[1] = $p;
            }
        } else {
            $str[1] = array();
            if (!empty($temp)) {
                $str[1] = $temp;
            } else {
                $str[1] = array();
            }
        }
        return $str;
    }

    function createUpdate($options) {
        $str = "UPDATE";
        $table = $options['tables'];
        $set = $this->createSet($options['sets']);
        $where = $this->createWhere($options['where']);
        $limit = $this->createLimit($options['limit']);
        $order = $this->createOrder($options['order']);
        return "$str $table $set $where $order $limit;";
    }

    function createSelect($options) {
        $columns = $this->createColumns($options['columns']);
        $from = $this->createFrom($options['tables']);
        $where = $this->createWhere($options['where']);
        $group = $this->createGroup($options['group']);
        $having = $this->createHaving($options['having']);
        $limit = $this->createLimit($options['limit']);
        $order = $this->createOrder($options['order']);
        return sprintf("SELECT %s %s %s %s %s %s %s;", $columns, $from, $where, $group, $having, $order, $limit);
    }

    function createDelete($options) {
        // delete from table where aaa=ccc limit 1;
        $table = $this->createFrom($options['tables']);
        $where = $this->createWhere($options['where']);
        $limit = $this->createLimit($options['limit']);
        $from = $this->createFrom($options['tables']);
        return "DELETE $from $where $limit";
    }

    function createInsert($options) {
        // insert into table (columns) values ();
        return "INSERT INTO " . $options['tables'] . $this->createValues($options['values']) . ";";
    }

    function createValues($values) {
        if (is_array($values)) {
            foreach ($values as $key => $val) {
                $cols[] = $key;
                $vals[] = "?";
            }
            return " (" . join(",", $cols) . ") VALUES (" . join(",", $vals) . ")";
        } else {
            return trim($values);
        }
    }

    function createSet($sets) {
        if (is_array($sets)) {
            foreach ($sets as $key => $val) {
                $arr[] = $key . '= ?';
            }
            return "SET " . join(",", $arr);
        } else {
            return "SET " . $sets;
        }
    }

    /**
     * 
     * @param $tables array|string
     * @return: string
     */
    function createFrom($tables) {
        if (is_array($tables)) {
            return "FROM " . join(",", $tables);
        } else {
            return "FROM " . $tables;
        }
    }

    /**
     * 目前暫不支援array, 請使用string語法直接帶入where敘述句
     * @param $where : where statament string 
     * @return : string
     */
    function createWhere($where) {
        if (is_array($where)) {
            foreach ($where as $key => $val) {
                $w[] = $key . '= ?';
            }
            return ' WHERE 1 AND ' . join(' AND ', $w);
        } elseif (strlen($where) >= 2) {
            return ' WHERE 1 AND ' . trim($where);
        } else {
            return " WHERE 1";
        }
    }

    /**
     * 
     * @param $order : an order string
     * @return :string
     * Example :
     * table1.ColumnA desc
     */
    function createOrder($order) {
        if (is_string($order) && strlen($order >= 0)) {
            return ' ORDER BY ' . $order;
        } else {
            return '';
        }
    }

    function createColumns($columns) {
        if (is_array($columns)) {
            return join(',', $columns);
        } elseif (strlen($columns) >= 0) {
            return trim($columns);
        } else {
            return '*';
        }
    }

    function createLimit($limit) {
        $limit = filter_var($limit, FILTER_VALIDATE_INT);
        if ($limit && strlen($limit) >= 0) {
            return "LIMIT " . $limit;
        } else {
            return '';
        }
    }

    function createGroup($group) {
        if (is_string($group)) {
            return "GROUP BY " . $group;
        } else {
            return '';
        }
    }

    function createHaving($having) {
        if (is_string($having)) {
            return "HAVING " . $having;
        } else {
            return '';
        }
    }

    function sqlInjectDeny($str) {
        return $str;
        return addslashes($str);
    }

    function _sqlInjectDeny($str) {
        return addslashes($str);
    }

	/**
	 * 多筆insert
	 * @param  string $table
	 * @param  array  $values
	 * @return boolean
	 */
	function multi_insert($table = '', $values = array()){
		if(count($values) === 0) {
			return false;
		}

		$rs = true;
		$value_tmp = array(0);
		foreach($values as $value) {
			$diffs = array_diff_key($value, $value_tmp);

			if(count($diffs) >= 1) {
				$options = array(
					'type' => 'insert',
					'tables' => $table,
					'values' => $value
				);
				$sql = $this->createInsert($options);
				$statement = $this->obj->prepare($sql);
				$value_tmp = $value;
			}
			$rs_tmp = $statement->execute(array_values($value));
			$rs = $rs && $rs_tmp;
		}
		return $rs;
	}
}

class Result {

    private $stmt;

    public function __construct($stmt) {
        $this->stmt = $stmt;
    }

    public function numRows() {
        return $this->stmt->rowCount();
    }

    public function fetchRow() {
        return $this->stmt->fetch();
    }

    public function errorCode() {
        return $this->stmt->errorCode();
    }

    public function getMessage() {
        $error_info = $this->stmt->errorInfo();
        return $error_info[2];
    }

    public function rowCount() {
        return $this->stmt->rowCount();
    }

}

class DB {

    public static function isError($result) {
        if (is_array($result)) {
            return true;
        } else {
            return $result->errorCode() != PDO::ERR_NONE;
        }
    }

}

?>
