<?php

class PDO_DB extends PDO {

    public function getAll($sql) {
        if (is_array($sql) && !empty($sql)) {
            $sqls = $sql[0];
            $params = $sql[1];
            $stmt = $this->prepare($sqls);
            if (!empty($params)) {
                $this->bindArray($stmt, $params);
            }
        } else {
            $stmt = $this->prepare($sql);
        }
        $stmt->execute();
        $result = $stmt->fetchAll();
        return $result;
    }

    public function getOne($sql) {
        if (is_array($sql) && !empty($sql)) {
            $sqls = $sql[0];
            $params = $sql[1];
            $stmt = $this->prepare($sqls);
            if (!empty($params)) {
                $this->bindArray($stmt, $params);
            }
        } else {
            $stmt = $this->prepare($sql);
        }
        $stmt->execute();
        $result = $stmt->fetchColumn();
        return $result;
    }

    public function getRow($sql) {
        if (is_array($sql) && !empty($sql)) {
            $sqls = $sql[0];
            $params = $sql[1];
            $stmt = $this->prepare($sqls);
            if (!empty($params)) {
                $this->bindArray($stmt, $params);
            }
        } else {
            $stmt = $this->prepare($sql);
        }
        $stmt->execute();
        $result = $stmt->fetch();
        return $result;
    }

    public static function isError($result) {
        if (is_array($result)) {
            return true;
        } else {
            return $result->errorCode() != PDO::ERR_NONE;
        }
    }
    /**
     * 綁定多個參數給 prepared statement
     * 
     * @param PDOStatement $stmt prepared statement
     * @param array $params 要綁定的參數
     */
    public function bindArray(&$stmt, $params) {
        if (is_array($params) && !empty($params)) {
            foreach ($params as $colum => $value) {
                $colum = $colum + 1;
                if (is_int($value)) {
                    $type = PDO::PARAM_INT;
                } else if (is_bool($value)) {
                    $type = PDO::PARAM_BOOL;
                } else if (is_null($value))
                    $type = PDO::PARAM_NULL;
                else {
                    $type = PDO::PARAM_STR;
                }
                $stmt->bindValue($colum, $value, $type);
            }
        }
    }   
}
