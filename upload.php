<?php
require_once 'libs/db.class.php';
require_once 'config.php';
/** Include PHPExcel */
require_once dirname(__FILE__) . '/Classes/PHPExcel.php';

$voc_id1 = $_POST["voc_id1"];
$voc_id2 = $_POST["voc_id2"];
$voc_id3 = $_POST["voc_id3"];
//Check valid spreadsheet has been uploaded
if(isset($_FILES['spreadsheet'])){
	if($_FILES['spreadsheet']['name']){
	    if(!$_FILES['spreadsheet']['error'])
	    {

	        $inputFile = $_FILES['spreadsheet']['name'];
	        $extension = strtoupper(pathinfo($inputFile, PATHINFO_EXTENSION));
	        if($extension == 'XLSX' || $extension == 'ODS'){

	            //Read spreadsheeet workbook
	            try {
					//php7
	            	PHPExcel_Settings::setZipClass(PHPExcel_Settings::ZIPARCHIVE);
	            	//php5主機現行
	            	//PHPExcel_Settings::setZipClass(PHPExcel_Settings::PCLZIP);
	                 $inputFile = $_FILES['spreadsheet']['tmp_name'];
	                 $inputFileType = PHPExcel_IOFactory::identify($inputFile);
	                 $objReader = PHPExcel_IOFactory::createReader($inputFileType);
	                 $objPHPExcel = $objReader->load($inputFile);
	            } catch(Exception $e) {
	                    die($e->getMessage());
	            }

	            //Get worksheet dimensions
	            $sheet = $objPHPExcel->getSheet(0); 
	            $highestRow = $sheet->getHighestRow(); 
	            $highestColumn = $sheet->getHighestColumn();
	            $data = array();
	            $lv6_array = array();
	            //Loop through each row of the worksheet in turn
	            for ($row = 2; $row <= $highestRow; $row++){ 
	                    //  Read a row of data into an array
            		//$msg = array();
                    $rowData = $sheet->rangeToArray('A' . $row . ':' . $highestColumn . $row, NULL, TRUE, FALSE);
                    $voc_name4 = $rowData[0][0];
                    $voc_name5 = $rowData[0][1];
                    $voc_name6 = $rowData[0][2];
                    if (!empty($voc_name4) && !empty($voc_name5) && !empty($voc_name6)) {
	                    $voc_id4 = "";
	                    $voc_id5 = "";
	                    $voc_id6 = "";
	                    $voc_org_value = "";
	                    $msg = "";
	                    $doc = array();
	                    $sql = "select voc_id from vocabulary where voc_level = 4 and voc_pid  = " . $voc_id3 . " and voc_name = '" . filter_var($voc_name4, FILTER_SANITIZE_STRING) . "'";
	                    $db = new MyDB($DSN);
	    				$rs = $db->obj->getRow($sql);
	    				if ($rs) {
	    					$voc_id4 = $rs['voc_id'];
	    					//$sql = "select voc_id from vocabulary where voc_id <> {$voc_id4} and voc_name = '{voc_name4}'";
	    				//} else {
	    					//$sql = "select voc_id from vocabulary where voc_name = '{voc_name4}'";
	    				}
	    				/*
					    $rs = $db->obj->getOne($sql);
					    if ($rs) {
					    	$msg .= "[顯示詞重覆]";
					    }
					    */
					    $sql = "select voc_id from vocabulary where voc_name = '{voc_name5}'"; 
					    if (!empty($voc_id4)) {
		                    $sql1 = "select voc_id from vocabulary where voc_level = 5 and voc_pid  = " . $voc_id4 . " and voc_name = '" . filter_var($voc_name5, FILTER_SANITIZE_STRING) . "'";
		    				$rs = $db->obj->getRow($sql1);
		    				if ($rs) {
		    					$voc_id5 = $rs['voc_id'];
		    					$sql .= "and voc_id <> {$voc_id5} ";
		    				}			    	
					    }
					    /*	    			
					    $rs = $db->obj->getOne($sql);
					    if ($rs) {
					    	$msg .= "[詞別重覆]";
					    }
					    */
					    $sql = "select voc_id from vocabulary where voc_name = '{voc_name6}'"; 
					    if (!empty($voc_id5)) {
		                    $sql1 = "select voc_id, voc_value from vocabulary where voc_level = 6 and voc_pid  = " . $voc_id5 . " and voc_name = '" . filter_var($voc_name6, FILTER_SANITIZE_STRING) . "'";
		    				$rs = $db->obj->getRow($sql1);
		    				if ($rs) {
		    					$voc_id6 = $rs['voc_id'];
		    					$voc_org_value = $rs['voc_value'];
		    					$sql .= "and voc_id <> {$voc_id6} ";
		    				}			    	
					    }	    			
					    $rs = $db->obj->getOne($sql);
					    /*
					    if ($rs) {
					    	$msg .= "[詞重覆]";
					    }
					    if (!in_array($voc_name6, $lv6_array)) {
					    	$lv6_array[] = $voc_name6;
					    } else {
					    	$msg .= "[文件內詞重覆]";
					    }
					    */
					    if (is_array($data)) {
					    	foreach ($data as $k => $v) {
					    		if ($v[0][2] == $voc_name6 ) {
					    			unset($data[$k]);
					    		}
					    	}					    	
					    }

					    $rowData[] = $voc_id4;
					    $rowData[] = $voc_id5;
					    $rowData[] = $voc_id6;
					    $rowData[] = $voc_org_value;
					    $rowData[] = $msg;
					    $data[] = $rowData;	                   	
                    }
			    	$data=array_values($data);	    					   				
	            }
	            echo json_encode($data);
	        }
	        else{
	            echo "Please upload an XLSX or ODS file";
	        }
	    }
	    else{
	        echo $_FILES['spreadsheet']['error'];
	    }
	}
}

?>