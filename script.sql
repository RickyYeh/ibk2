USE [master]
GO
/****** Object:  Database [IBK]    Script Date: 2018/9/20 下午 04:25:04 ******/
CREATE DATABASE [IBK]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'IBK', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.SQLEXPRESS\MSSQL\DATA\IBK.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'IBK_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL13.SQLEXPRESS\MSSQL\DATA\IBK_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
GO
ALTER DATABASE [IBK] SET COMPATIBILITY_LEVEL = 130
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [IBK].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [IBK] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [IBK] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [IBK] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [IBK] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [IBK] SET ARITHABORT OFF 
GO
ALTER DATABASE [IBK] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [IBK] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [IBK] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [IBK] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [IBK] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [IBK] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [IBK] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [IBK] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [IBK] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [IBK] SET  DISABLE_BROKER 
GO
ALTER DATABASE [IBK] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [IBK] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [IBK] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [IBK] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [IBK] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [IBK] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [IBK] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [IBK] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [IBK] SET  MULTI_USER 
GO
ALTER DATABASE [IBK] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [IBK] SET DB_CHAINING OFF 
GO
ALTER DATABASE [IBK] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [IBK] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [IBK] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [IBK] SET QUERY_STORE = OFF
GO
USE [IBK]
GO
ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO
ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO
ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO
USE [IBK]
GO
/****** Object:  Table [dbo].[glossary]    Script Date: 2018/9/20 下午 04:25:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[glossary](
	[glo_id] [int] IDENTITY(1,1) NOT NULL,
	[glo_tag_id] [int] NOT NULL,
	[glo_voc_id] [int] NOT NULL,
	[glo_voc_name] [varchar](50) NULL,
	[glo_flag] [tinyint] NULL,
	[glo_create_user] [varchar](16) NULL,
	[glo_create_time] [datetime] NULL,
	[glo_update_user] [varchar](16) NULL,
	[glo_update_time] [datetime] NULL,
 CONSTRAINT [PK_glossary] PRIMARY KEY CLUSTERED 
(
	[glo_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[project]    Script Date: 2018/9/20 下午 04:25:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[project](
	[pro_id] [int] IDENTITY(1,1) NOT NULL,
	[pro_name] [nvarchar](50) NOT NULL,
	[pro_usr_id] [int] NULL,
	[pro_create_user] [varchar](16) NULL,
	[pro_create_time] [datetime] NULL,
	[pro_update_user] [varchar](16) NULL,
	[pro_update_time] [datetime] NULL,
 CONSTRAINT [PK_project] PRIMARY KEY CLUSTERED 
(
	[pro_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[tag]    Script Date: 2018/9/20 下午 04:25:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tag](
	[tag_id] [int] IDENTITY(1,1) NOT NULL,
	[tag_pro_id] [int] NOT NULL,
	[tag_name] [nvarchar](50) NOT NULL,
	[tag_create_user] [varchar](16) NULL,
	[tag_create_time] [datetime] NULL,
	[tag_update_user] [varchar](16) NULL,
	[tag_update_time] [datetime] NULL,
 CONSTRAINT [PK_tag] PRIMARY KEY CLUSTERED 
(
	[tag_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[user]    Script Date: 2018/9/20 下午 04:25:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[user](
	[usr_id] [int] IDENTITY(1,1) NOT NULL,
	[usr_account] [varchar](16) NOT NULL,
	[usr_password] [varchar](16) NOT NULL,
	[usr_name] [nvarchar](50) NULL,
	[usr_type] [tinyint] NOT NULL,
	[usr_create_user] [varchar](16) NULL,
	[usr_create_time] [datetime] NULL,
	[usr_update_user] [varchar](16) NULL,
	[usr_update_time] [datetime] NULL,
 CONSTRAINT [PK_user] PRIMARY KEY CLUSTERED 
(
	[usr_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[vocabulary]    Script Date: 2018/9/20 下午 04:25:04 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[vocabulary](
	[voc_id] [int] IDENTITY(1,1) NOT NULL,
	[voc_pid] [int] NOT NULL,
	[voc_name] [nvarchar](50) NOT NULL,
	[voc_level] [tinyint] NOT NULL,
	[voc_value] [int] NULL,
	[voc_create_user] [varchar](16) NULL,
	[voc_create_time] [datetime] NULL,
	[voc_update_user] [varchar](16) NULL,
	[voc_update_time] [datetime] NULL,
 CONSTRAINT [PK__vocabula__AFD607F6D1DF9C8B] PRIMARY KEY CLUSTERED 
(
	[voc_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Index [IX_glossary_tag_id]    Script Date: 2018/9/20 下午 04:25:04 ******/
CREATE NONCLUSTERED INDEX [IX_glossary_tag_id] ON [dbo].[glossary]
(
	[glo_tag_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_project_usr_id]    Script Date: 2018/9/20 下午 04:25:04 ******/
CREATE NONCLUSTERED INDEX [IX_project_usr_id] ON [dbo].[project]
(
	[pro_usr_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_tag]    Script Date: 2018/9/20 下午 04:25:04 ******/
CREATE NONCLUSTERED INDEX [IX_tag] ON [dbo].[tag]
(
	[tag_pro_id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_vocabulary_level]    Script Date: 2018/9/20 下午 04:25:04 ******/
CREATE NONCLUSTERED INDEX [IX_vocabulary_level] ON [dbo].[vocabulary]
(
	[voc_level] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_vocabulary_pid]    Script Date: 2018/9/20 下午 04:25:04 ******/
CREATE NONCLUSTERED INDEX [IX_vocabulary_pid] ON [dbo].[vocabulary]
(
	[voc_pid] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
/****** Object:  Index [IX_vocabulary_update_time]    Script Date: 2018/9/20 下午 04:25:04 ******/
CREATE NONCLUSTERED INDEX [IX_vocabulary_update_time] ON [dbo].[vocabulary]
(
	[voc_update_time] DESC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
GO
ALTER TABLE [dbo].[glossary] ADD  CONSTRAINT [DF_Table_1_voc_pid]  DEFAULT ((0)) FOR [glo_tag_id]
GO
ALTER TABLE [dbo].[glossary] ADD  CONSTRAINT [DF_glossary_blo_flag]  DEFAULT ((0)) FOR [glo_flag]
GO
ALTER TABLE [dbo].[glossary] ADD  CONSTRAINT [DF_glossary_glo_create_time]  DEFAULT (getdate()) FOR [glo_create_time]
GO
ALTER TABLE [dbo].[glossary] ADD  CONSTRAINT [DF_glossary_glo_update_time]  DEFAULT (getdate()) FOR [glo_update_time]
GO
ALTER TABLE [dbo].[project] ADD  CONSTRAINT [DF_project_pro_lv1_id]  DEFAULT ((0)) FOR [pro_usr_id]
GO
ALTER TABLE [dbo].[project] ADD  CONSTRAINT [DF_project_pro_create_time]  DEFAULT (getdate()) FOR [pro_create_time]
GO
ALTER TABLE [dbo].[project] ADD  CONSTRAINT [DF_project_pro_update_time]  DEFAULT (getdate()) FOR [pro_update_time]
GO
ALTER TABLE [dbo].[tag] ADD  CONSTRAINT [DF_tag_tag_pro_id]  DEFAULT ((0)) FOR [tag_pro_id]
GO
ALTER TABLE [dbo].[tag] ADD  CONSTRAINT [DF_tag_tag_create_time]  DEFAULT (getdate()) FOR [tag_create_time]
GO
ALTER TABLE [dbo].[tag] ADD  CONSTRAINT [DF_tag_tag_update_time]  DEFAULT (getdate()) FOR [tag_update_time]
GO
ALTER TABLE [dbo].[user] ADD  CONSTRAINT [DF_user_usr_type]  DEFAULT ((0)) FOR [usr_type]
GO
ALTER TABLE [dbo].[user] ADD  CONSTRAINT [DF_user_usr_create_time]  DEFAULT (getdate()) FOR [usr_create_time]
GO
ALTER TABLE [dbo].[user] ADD  CONSTRAINT [DF_user_usr_update_time]  DEFAULT (getdate()) FOR [usr_update_time]
GO
ALTER TABLE [dbo].[vocabulary] ADD  CONSTRAINT [DF_vocabulary_voc_pid]  DEFAULT ((0)) FOR [voc_pid]
GO
ALTER TABLE [dbo].[vocabulary] ADD  CONSTRAINT [DF_vocabulary_voc_level]  DEFAULT ((0)) FOR [voc_level]
GO
ALTER TABLE [dbo].[vocabulary] ADD  CONSTRAINT [DF_vocabulary_voc_value]  DEFAULT ((0)) FOR [voc_value]
GO
ALTER TABLE [dbo].[vocabulary] ADD  CONSTRAINT [DF_vocabulary_voc_create_time]  DEFAULT (getdate()) FOR [voc_create_time]
GO
ALTER TABLE [dbo].[vocabulary] ADD  CONSTRAINT [DF_vocabulary_voc_update_time]  DEFAULT (getdate()) FOR [voc_update_time]
GO
USE [master]
GO
ALTER DATABASE [IBK] SET  READ_WRITE 
GO
