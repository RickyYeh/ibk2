<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.8.0, 2014-03-02
 */

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Asia/Taipei');


if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');
require_once 'libs/db.class.php';
require_once 'config.php';

/** Include PHPExcel */
require_once dirname(__FILE__) . '/Classes/PHPExcel.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("IBK")
							 ->setLastModifiedBy("IBK")
							 ->setTitle("PHPExcel Test Document")
							 ->setSubject("PHPExcel Test Document")
							 ->setDescription("Test document for PHPExcel, generated using PHP classes.")
							 ->setKeywords("office PHPExcel php")
							 ->setCategory("Test result file");							 

/* Performing SQL query */
$level = 3;
$voc_id = $_GET["pid"];
$db = new MyDB($DSN);
$where = " where lv" . $level . ".voc_id=" . $voc_id;
$sql = "select lv1.voc_name as voc_name1, lv2.voc_name as voc_name2, lv3.voc_name as voc_name3 from (
		(select voc_id, voc_name from vocabulary where voc_level = 1) lv1 
		inner join (select voc_id, voc_pid, voc_level, voc_name from vocabulary where voc_level = 2 ) lv2 on lv1.voc_id = lv2.voc_pid  
		inner join (select voc_id, voc_pid, voc_level, voc_name from vocabulary where voc_level = 3 ) lv3 on lv2.voc_id = lv3.voc_pid)" . $where;
$rs = $db->obj->getRow($sql);		
$parent_name = $rs['voc_name1'] . "_" . $rs['voc_name2'] . "_" . $rs['voc_name3'];		 
//$where = " where lv" . $level . ".voc_id=" . $voc_id;
$sql = "select lv1.voc_name as voc_name1, lv2.voc_name as voc_name2, lv3.voc_name as voc_name3, lv4.voc_name as voc_name4, lv5.voc_name as voc_name5, lv6.voc_name as voc_name6, lv6.voc_value  from (
		(select voc_id, voc_name from vocabulary where voc_level = 1) lv1 
		inner join (select voc_id, voc_pid, voc_level, voc_name from vocabulary where voc_level = 2 ) lv2 on lv1.voc_id = lv2.voc_pid  
		inner join (select voc_id, voc_pid, voc_level, voc_name from vocabulary where voc_level = 3 ) lv3 on lv2.voc_id = lv3.voc_pid  
		inner join (select voc_id, voc_pid, voc_level, voc_name from vocabulary where voc_level = 4 ) lv4 on lv3.voc_id = lv4.voc_pid 
		inner join (select voc_id, voc_pid, voc_level, voc_name from vocabulary where voc_level = 5 ) lv5 on lv4.voc_id = lv5.voc_pid 
		inner join (select voc_id, voc_pid, voc_level, voc_name, voc_value from vocabulary where voc_level = 6 ) lv6 on lv5.voc_id = lv6.voc_pid 
		)" . $where;


$rs = $db->obj->getAll($sql);	

$i = 1;
$objPHPExcel->setActiveSheetIndex(0)
		->setCellValue('A' . $i, '顯示詞')
		->setCellValue('B' . $i, '詞別')
		->setCellValue('C' . $i, '詞')
		->setCellValue('D' . $i, '數值');	
		
foreach ($rs as $key => $value) {
	$no = $i;
	$i++;
	
	$objPHPExcel->setActiveSheetIndex(0)
				//->setCellValue('A' . $i, $no)
				->setCellValue('A' . $i, $value['voc_name4'])
				->setCellValue('B' . $i, $value['voc_name5'])
				->setCellValue('C' . $i, $value['voc_name6'])
				->setCellValue('D' . $i, $value['voc_value']);	
}

$lastColumn = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
$lastColumn++;
for ($column = 'A'; $column != $lastColumn; $column++) {
	$objPHPExcel->getActiveSheet()
	    ->getStyle($column . '1')
	    ->getFill()
	    ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
	    ->getStartColor()
	    ->setARGB('FFCCCCCC');
	$objPHPExcel->getActiveSheet()->getStyle($column . '1')->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);    	
}

$timestamp = date("YmdHis");
$filename = $parent_name;
$filename .= "_" . $timestamp . ".xlsx";
//$filename = iconv('UTF-8','Big5',$filename);
// Add some data
/*
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Hello')
            ->setCellValue('B2', 'world!')
            ->setCellValue('C1', 'Hello')
            ->setCellValue('D2', 'world!');
*/
// Miscellaneous glyphs, UTF-8
/*
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A4', 'Miscellaneous glyphs')
            ->setCellValue('A5', 'éàèùâêîôûëïüÿäöüç');
*/
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle($parent_name);


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

ob_end_clean();
// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=utf-8');
header('Content-Disposition: attachment;filename="' . $filename . '"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
