﻿<?php
require_once 'common.php';

if ($_SESSION['login_status'] == "") header( 'Location: login.php' );
if ($_SESSION['usr_type'] <> 1 && $_SESSION['usr_type'] <> 2) header( 'Location: index.php' );
?>

<!--[if IE ]><![endif]-->
<!doctype html>
<!--[if IE 8 ]> <html class="no-js lt-ie9 ie8" lang="zh-TW"> <![endif]-->
<!--[if IE 9 ]> <html class="no-js lt-ie10 ie9" lang="zh-TW"> <![endif]-->
<!--[if (gte IE 10)|!(IE)]><!-->
<html lang="zh-TW">
<!--<![endif]-->

<head>
	<title><?php echo TITLE?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/bootstrap-theme.css">
	<link rel="stylesheet" href="css/bootstrap-select.css">
	<link rel="stylesheet" href="css/main.css?1540876269">
	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="js/bootstrap.js"></script>
	<script src="js/bootstrap-select.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css">
  	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
	<script type="text/javascript" charset="utf8" src="js/jquery.dataTables.js"></script>		
	<script src="js/main.js?1513089943"></script>
	<script type="text/javascript" src="js/jquery.validate.js"></script>
	<script type="text/javascript" src="js/messages_zh_TW.js"></script>
	<script type="text/javascript" src="js/additional-methods.js"></script>	
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" />
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css" />	
</head>

<body>
	<div class="wrap">
		<div class="header_wrap">
 <?php include ('nav.php'); ?>
		</div>
		<div class="content_wrap">
			<div class="row">
				<div class="col-md-10 col-md-offset-1">
					<div class="dropdown-area">
						<select class="selectpicker dropdown-level" data-style="btn-mute" id="dropdown_lv1" data-level="1">
						  <option value="0">--請選擇--</option>
						</select>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-10 col-md-offset-1">				
					<table id="table_import" class="display">
					    <thead>
					        <tr>
					            <th class="text-center">編號</th>
					            <th class="text-center">名稱</th>
					            <th class="text-center">最新修改時間</th>
					            <th class="text-center">維護</th>
					            <th class="text-center">操作</th>					            					            
					        </tr>
					    </thead>
					    <tbody>

					    </tbody>
					</table>
					<table id="table_import_confirm" class="display" style="width:100%">
					    <thead>
					        <tr>
					            <th class="text-center">編號</th>
					            <th class="text-center">顯示詞</th>
					            <th class="text-center">詞別</th>
					            <th class="text-center">詞</th>
					            <th class="text-center">原數值</th>
					            <th class="text-center">新數值</th>
					            <!--<th class="text-center">說明</th>-->
					            <th class="text-center">ID4</th>
					            <th class="text-center">ID5</th>
					            <th class="text-center">ID6</th>
					            <th class="text-center">全選 <input type="checkbox" name="chk_all" id="chk_all"></th>
					        </tr>
					    </thead>
					    <tbody>

					    </tbody>
					</table>
					<button id="btn_import" class="btn btn-primary pull-right">匯入</button>					
				</div>
				
			</div>									
		</div>
		<div class="row">
			<div class="col-md-10 col-md-offset-1">	
				<div id="import_message" class="text-center"><div></div></div>
			</div>	
		</div>			
		<footer class="footer">

		</footer>
	</div>
<!-- Modal -->
<div id="uploadModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">匯入詞庫</h4>
      </div>
      <div class="modal-body">
		<form id="form_upload" enctype="multipart/form-data">
		  <div class="form-group">
		    <label for="usr_account">檔案:</label>
		    <input type="file" class="form-control" name="upload_file" id="upload_file">
		  </div>
		  <input type="hidden" name="voc_id1" id="voc_id1">
		  <input type="hidden" name="voc_id2" id="voc_id2">
		  <input type="hidden" name="voc_id3" id="voc_id3">
		  <button type="submit" class="btn btn-primary">上傳</button>
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>	
</body>
</html>