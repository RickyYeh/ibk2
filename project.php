﻿<?php
require_once 'common.php';

if ($_SESSION['login_status'] == "") header( 'Location: login.php' );
?>

<!--[if IE ]><![endif]-->
<!doctype html>
<!--[if IE 8 ]> <html class="no-js lt-ie9 ie8" lang="zh-TW"> <![endif]-->
<!--[if IE 9 ]> <html class="no-js lt-ie10 ie9" lang="zh-TW"> <![endif]-->
<!--[if (gte IE 10)|!(IE)]><!-->
<html lang="zh-TW">
<!--<![endif]-->

<head>
	<title><?php echo TITLE?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/bootstrap-theme.css">
	<link rel="stylesheet" href="css/bootstrap-select.css">
	<link rel="stylesheet" href="css/main.css?1540876269">
	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="js/bootstrap.js"></script>
	<script src="js/bootstrap-select.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css">
  	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
	<script type="text/javascript" charset="utf8" src="js/jquery.dataTables.js"></script>		
	<script src="js/main.js?1513089943"></script>
	<script src="js/jquery.cookie.js"></script>
	<script type="text/javascript" src="js/jquery.validate.js"></script>
	<script type="text/javascript" src="js/messages_zh_TW.js"></script>
	<script type="text/javascript" src="js/additional-methods.js"></script>		
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" />
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css" />	
</head>

<body>
	<div class="wrap">
		<div class="header_wrap">
 <?php include ('nav.php'); ?>
		</div>
		<div class="content_wrap">
			<ul class="nav nav-tabs">
	  			<li class="active"><a data-toggle="tab" href="#save_panel">儲存專案</a></li>
	  			<li class="" id="tab_admin_project"><a data-toggle="tab" href="#admin_panel">專案管理</a></li>
	  		</ul>	
	  		<div class="tab-content">
				<div id="save_panel" class="tab-pane fade in active">
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							<div class="dropdown-area col-md-6">
								<select class="selectpicker dropdown-level" data-style="btn-mute" id="dropdown_lv1" data-level="1" style="margin-top:-10px">
								  <option value="0">--請選擇--</option>
								</select>
								<select class="selectpicker dropdown-level" data-style="btn-mute" id="dropdown_lv2" data-level="2" style="margin-top:-10px">
								  <option value="0">--請選擇--</option>
								</select>
                            </div>
							<div class="col-md-6">

								<div id="imaginary_container" class="pull-right"> 
					                <div class="input-group stylish-input-group">
					                    <input type="text" class="form-control"  placeholder="Search" id="keyword">
					                    <span class="input-group-addon">
					                        <button type="submit" id="btn_search">
					                            <span class="glyphicon glyphicon-search"></span>
					                        </button>  
					                    </span>
					                </div>

					            </div>
                                <button type="button" id="save_public_project_btn" class="btn btn-info pull-right" style="margin:29px 10px">
                                    儲存公版專案
                                </button>
		            		</div>					
						</div>
					</div>
				
					<div class="row">
						<div class="col-md-10 col-md-offset-1" id="table_div">				
							<table id="table_import" class="display table table-striped">
							    <thead>
							        <tr>
							            <th class="text-center">編號</th>
							            <th class="text-center">預設詞庫名稱</th>
							            <th class="text-center">最新修改時間</th>
							            <th class="text-center">維護</th>
							            <th class="text-center">操作</th>				            					            
							        </tr>
							    </thead>
							    <tbody>
							    </tbody>
							</table>			
						</div>
						<div class="col-md-10 col-md-offset-1 text-center" style="border-top:1px solid #cccccc">
							<a href="javascript:;"><span class="glyphicon glyphicon-chevron-up" id="toggle_arrow"></span></a>
						</div>
					</div>
					
					<div class="row" style="min-width: 992px">
				        <div class="col-lg-7 col-lg-offset-1 col-md-10 col-sm-10 col-xs-10">

				                
		          
				            <!-- Nav tabs -->
				            <ul id="tab-list" class="nav nav-tabs" role="tablist">
				                <li class="active"><a href="#tab1" role="tab" data-toggle="tab"><span>Tab 1 </span><span class="glyphicon glyphicon-pencil text-muted edit"></span></a></li>
				            </ul>

				            <!-- Tab panes -->
				            <div id="tab-content" class="tab-content">
				                <div class="tab-pane fade in active" id="tab1">
				                	<div class="row">
				                		<div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 text-right">
								        	<select name="lv4[]" id="lv4_1" multiple size="10">
									        </select>
									        <button id="btn_remove_4_1" class="btn btn-danger btn-remove-4"><span class="glyphicon glyphicon-remove"></span></button>
								    	</div>
								    	<div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 text-center">
								        	<button id="btn_send_1" class="btn btn-info btn-send"><span class="glyphicon glyphicon-chevron-right"></span></button>
								        </div>
								        <div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="width:250px">
									        <select name="lv4s[]" id="lv4s_1" multiple size="10">
									        </select>
									        <button id="btn_remove_4s_1" class="btn btn-danger btn-remove-4s"><span class="glyphicon glyphicon-remove"></span></button>
								    	</div>
							    	</div>
				                </div>
				            </div>
				        </div>
				        <div class="col-md-1 col-sm-1 col-xs-1">
				        	<ul id="tab_button_group">
					        	<li><button id="btn-add-tab" type="button" class="btn btn-primary" style="width:120px"><span class="glyphicon glyphicon glyphicon-plus"></span> 新增自訂詞庫</button></li>
					           	<!--<li><button id="btn-tab-export" type="button" class="btn btn-primary">統計詞匯出</button></li>-->
				           	</ul> 
				        </div>		        
				        <div class="col-md-2">
					        <ul id="project_panel" class="pull-right">
					        	
					        	<li><button class="btn btn-default" id="btn_save">儲存</button></li>
								<li><button class="btn btn-default" data-toggle="modal" data-target="#loadModal" id="btn_load_project">開啟</button></li>
					        	<li><button class="btn btn-default" data-toggle="modal" data-target="#saveModal" id="btn_saveas">另存新檔</button></li>
					        </ul>
				        </div>
					</div>

							
				</div>
				<div id="admin_panel" class="tab-pane fade">
					<div class="row">
						<div class="col-md-10 col-md-offset-1">
							<table id="table_project" class="display table table-striped">
							    <thead>
							        <tr>
							            <th class="text-center">序號</th>
							            <th class="text-center">專案名稱</th>
							            <th class="text-center">專案成立時間</th>
							            <th class="text-center">最後更新時間</th>
							            <th class="text-center">專案代號</th>
							            <th class="text-center">狀態</th>
							            <th class="text-center">管理</th>				            					            
							        </tr>
							    </thead>
							    <tbody>
							    </tbody>
							</table>		
						</div>
					</div>
					<div class="row" id="panel_project_detail">
						<div class="col-md-10 col-md-offset-1">
							<h4 id="project_title"></h4>
							<select class="selectpicker" id="project_group" data-style="btn-mute" >
								  <option value="">群組名稱(全選)</option>
								</select>
								<button class="btn text-right" id="btn_del_glossary">刪除勾選詞彙</button>
							<table id="table_project_detail" class="display table table-striped" style="margin-top:10px">
							    <thead>
							        <tr>
							            <th class="text-center">勾選 <input type="checkbox" id="chk_all_project_detail"></th>
							            <th class="text-center">流水號</th>
							            <th class="text-center">自訂詞庫名稱</th>
							            <th class="text-center">顯示詞</th>		            					            
							        </tr>
							    </thead>
							    <tbody>
							    </tbody>
							</table>
						</div>		
					</div>		
				</div>
			</div>
		</div>
		<footer class="footer">

		</footer>
	</div>
<!-- Modal -->
<div id="saveModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">儲存專案</h4>
      </div>
      <div class="modal-body">
		<form id="form_project">
		  <div class="form-group">
		    <label for="pro_name">專案名稱:</label>
		    <input type="text" class="form-control" name="pro_name" id="pro_name">
		  </div>
		  <button type="submit" class="btn btn-primary">儲存</button>
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<div id="loadModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">開啟專案</h4>
      </div>
      <div class="modal-body">

		    <div><select class="selectpicker bs-select-hidden" data-style="btn-mute" id="dropdown_project"></select></div>
		  
		  <button type="button" class="btn btn-primary" id="btn_load" style="margin:20px">開啟</button>
		
		<div id="project_message">尚無儲存專案可開啟!</div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>
<div id="previewModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">[<span id="title_name"></span>] 顯示詞預覽</h4>
      </div>
      <div class="modal-body">
      	<table class="table table-striped" id="preview_lv4">		
      		<tbody>
      			
      		</tbody>
      	</table>	
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>	
</body>
</html>