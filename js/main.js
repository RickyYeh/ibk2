var WORD_TYPE = ['大類', '子分類', '詞庫', '顯示詞', '詞別', '詞'];
var USER_TYPE = ['維運管理員', '知識組組員', '非知識組組員'];
var active = 0;
var pro_id = "";
var pro_name = "";
var lv5_name = "";
var url = window.location.pathname;
var filename = url.substring(url.lastIndexOf('/')+1);
$(function () {
	if ($("form.login").length) {
		$("form.login").validate({
			rules: {
				userAccount: {
					required: true
				}, 
				userPassword: {
					required: true
				},
			}, 
			messages: {
				userAccount:{
					required: "請輸入帳號!",
				},
				userPassword:{
					required: "請輸入密碼!",
				}			
			}, 
			onkeyup: false,
			onfocusout: false,
			showErrors: function(errorMap, errorList) {  
				/*
				var msg = "";  
				$.each( errorList, function(i,v){  
				 msg += (v.message+"\r\n");  
				});  
				if(msg!="") alert(msg);  
				*/
			},  		
			invalidHandler: function(form, validator) {
				var errors = validator.numberOfInvalids();
				if (errors) {
				
					var errors = "";
					if (validator.errorList.length > 0) {
						for (x=0;x<validator.errorList.length;x++) {
							errors += "\n\u25CF " + validator.errorList[x].message;
						}
					}
					alert(errors);
				}
				validator.focusInvalid();
			},
		    submitHandler: function(form) {
		        $.ajax({
		            url: "chk.php",
		            type: "post",
		            data: $(form).serialize(),
		            success: function(response) {
		            	if (response == "OK") {
		            		location.href="index.php";
		            	} else {
		            		alert(response);
		            	}
		               
		            }            
		        });
		    }		
		});	
	}
	$("#voc_type").on("change", function () {
		var lv = $( "#voc_type option:selected" ).val() - 1;

		if (lv > 0) {
			$(".dropdown-level").empty().append("<select><option>" + lv + "</option></select>");
		}
	})
	/**
	  We're defining the event on the `body` element, 
	  because we know the `body` is not going away.
	  Second argument makes sure the callback only fires when 
	  the `click` event happens only on elements marked as `data-editable`
	*/	
	$('body').on('click', '[data-editable]', function(){
	  
	  var $el = $(this);
	              
	  var $input = $('<input/>').val( $el.text() );
	  $el.replaceWith( $input );
	  
	  var save = function(){
	    var $p = $('<p data-editable />').text( $input.val() );
	    $input.replaceWith( $p );
	  };
	  
	  /**
	    We're defining the callback with `one`, because we know that
	    the element will be gone just after that, and we don't want 
	    any callbacks leftovers take memory. 
	    Next time `p` turns into `input` this single callback 
	    will be applied again.
	  */
	  $input.one('blur', save).focus();
	  
	});	
	//if ($(".nav li:eq(0)").hasClass("active")) {
	if (filename == "index.php") {	
		active = 0;
	    var labels = [];
	    var values = [];
	    var colors = [];
	    var ids = [];
	    var total = 0;
	    var default_lv1 = 0;
        $.ajax({
            url: "api.php",
            type: "post",
            data: {act : 'statistics'},
            success: function(response) {
            	var json = JSON.parse(response);
        		$.each(json, function(i, item) {
        			if (i == 0) {
        				$("#lv1_stat").text(item.voc_name);
        				default_lv1 = item.id1;
        			}
        			labels.push(item.voc_name);
        			values.push(item.cnt);
        			ids.push(item.id1);
        			total+=parseInt(item.cnt);
					r = Math.floor(Math.random() * 250);
					g = Math.floor(Math.random() * 250);
					b = Math.floor(Math.random() * 250);
					color = 'rgb(' + r + ', ' + g + ', ' + b + ')';
					colors.push(color);
				})	
				getLv1Stat(default_lv1);
      			$("#total_lv6").text(total);

 Chart.defaults.doughnutLabels = Chart.helpers.clone(Chart.defaults.pie);

var helpers = Chart.helpers;
var defaults = Chart.defaults;

Chart.controllers.doughnutLabels = Chart.controllers.doughnut.extend({
	updateElement: function(arc, index, reset) {
    var _this = this;
    var chart = _this.chart,
        chartArea = chart.chartArea,
        opts = chart.options,
        animationOpts = opts.animation,
        arcOpts = opts.elements.arc,
        centerX = (chartArea.left + chartArea.right) / 2,
        centerY = (chartArea.top + chartArea.bottom) / 2,
        startAngle = opts.rotation, // non reset case handled later
        endAngle = opts.rotation, // non reset case handled later
        dataset = _this.getDataset(),
        circumference = reset && animationOpts.animateRotate ? 0 : arc.hidden ? 0 : _this.calculateCircumference(dataset.data[index]) * (opts.circumference / (2.0 * Math.PI)),
        innerRadius = reset && animationOpts.animateScale ? 0 : _this.innerRadius,
        outerRadius = reset && animationOpts.animateScale ? 0 : _this.outerRadius,
        custom = arc.custom || {},
        valueAtIndexOrDefault = helpers.getValueAtIndexOrDefault;

    helpers.extend(arc, {
      // Utility
      _datasetIndex: _this.index,
      _index: index,

      // Desired view properties
      _model: {
        x: centerX + chart.offsetX,
        y: centerY + chart.offsetY,
        startAngle: startAngle,
        endAngle: endAngle,
        circumference: circumference,
        outerRadius: outerRadius,
        innerRadius: innerRadius,
        label: valueAtIndexOrDefault(dataset.label, index, chart.data.labels[index])
      },

      draw: function () {
      	var ctx = this._chart.ctx,
						vm = this._view,
						sA = vm.startAngle,
						eA = vm.endAngle,
						opts = this._chart.config.options;
				
					var labelPos = this.tooltipPosition();
					var segmentLabel = vm.circumference / opts.circumference * 100;
					
					ctx.beginPath();
					
					ctx.arc(vm.x, vm.y, vm.outerRadius, sA, eA);
					ctx.arc(vm.x, vm.y, vm.innerRadius, eA, sA, true);
					
					ctx.closePath();
					ctx.strokeStyle = vm.borderColor;
					ctx.lineWidth = vm.borderWidth;
					
					ctx.fillStyle = vm.backgroundColor;
					
					ctx.fill();
					ctx.lineJoin = 'bevel';
					
					if (vm.borderWidth) {
						ctx.stroke();
					}
					
					if (vm.circumference > 0.15) { // Trying to hide label when it doesn't fit in segment
						ctx.beginPath();
						ctx.font = helpers.fontString(opts.defaultFontSize, opts.defaultFontStyle, opts.defaultFontFamily);
						ctx.fillStyle = "#fff";
						ctx.textBaseline = "top";
						ctx.textAlign = "center";
            
            // Round percentage in a way that it always adds up to 100%
						ctx.fillText(segmentLabel.toFixed(0) + "%", labelPos.x, labelPos.y);
					}
      }
    });

    var model = arc._model;
    model.backgroundColor = custom.backgroundColor ? custom.backgroundColor : valueAtIndexOrDefault(dataset.backgroundColor, index, arcOpts.backgroundColor);
    model.hoverBackgroundColor = custom.hoverBackgroundColor ? custom.hoverBackgroundColor : valueAtIndexOrDefault(dataset.hoverBackgroundColor, index, arcOpts.hoverBackgroundColor);
    model.borderWidth = custom.borderWidth ? custom.borderWidth : valueAtIndexOrDefault(dataset.borderWidth, index, arcOpts.borderWidth);
    model.borderColor = custom.borderColor ? custom.borderColor : valueAtIndexOrDefault(dataset.borderColor, index, arcOpts.borderColor);

    // Set correct angles if not resetting
    if (!reset || !animationOpts.animateRotate) {
      if (index === 0) {
        model.startAngle = opts.rotation;
      } else {
        model.startAngle = _this.getMeta().data[index - 1]._model.endAngle;
      }

      model.endAngle = model.startAngle + model.circumference;
    }

    arc.pivot();
  }
});
  			
				var data = {
				  datasets: [{
				    data: values,
				    backgroundColor: colors,
				    ids: ids
				  }],
				  labels:labels
				};		
			    var canvas = document.getElementById("myChart");
			    var ctx = canvas.getContext("2d");
			    var myNewChart = new Chart(ctx, {
			      type: 'doughnutLabels',
			      data: data, 
			      options: {
			      	defaultFontSize: 20,
			      	labels: {
					      fontSize: 20
					  },
					tooltips: {
						bodyFontSize: 16
					}  
			      },
			      legend: {
			      	options: {
			      		labels: {
			      			fontSize: 20
			      		}
			      	},
			      	top: -10
			      },
			      responsive: true,
				    animation: {
				      animateScale: true,
				      animateRotate: true
				    }
			    });
			    myNewChart.legend.options.labels.fontSize = 16;
				myNewChart.legend.top = -10;
			    //myNewChart.options.defaultFontSize = 20;
			    canvas.onclick = function(evt) {
			      var activePoints = myNewChart.getElementsAtEvent(evt);
			      if (activePoints[0]) {
			        var chartData = activePoints[0]['_chart'].config.data;
			        var idx = activePoints[0]['_index'];

			        var label = chartData.labels[idx];
			        var value = chartData.datasets[0].data[idx];
			        var ids = chartData.datasets[0].ids[idx];

			        $("#lv1_stat").text(label);
			        getLv1Stat(ids);
			      }
			    };			    
            }          
        });		
        $.ajax({
            url: "api.php",
            type: "post",
            data: {act : 'updateStatus'},
			dataType:"json",
			success: function(data, textStatus, jqXHR) {
				$.each(data, function(index, item) {
					item.no = index+1; 
				})
				cols = [
		        {"data": "no"},
		        {"data": "lv1"},
		        {"data": "lv2"},
		        {"data": "voc_update_time"},
		        {"data": "voc_update_user"}   
		   		 ];	            	
				var opt={"oLanguage":{"sProcessing":"處理中...",
                                 "sLengthMenu":"顯示 _MENU_ 筆資料",
                                 "sZeroRecords":"沒有匹配結果",
                                 "sInfo":"顯示第 _START_ 至 _END_ 項結果，共 _TOTAL_ 項",
                                 "sInfoEmpty":"顯示第 0 至 0 筆資料，共 0 項",
                                 "sInfoFiltered":"(從 _MAX_ 項資料過濾)",
                                 "sSearch":"搜索:",
                                 "oPaginate":{"sFirst":"首頁",
                                                      "sPrevious":"上頁",
                                                      "sNext":"下頁",
                                                      "sLast":"尾頁"}
                                 },
                                 "processing": true,
                                 "bSort": false,
                                 "searching": false,
                                 "lengthChange": false,
                                 //"lengthMenu": [[1, 25, 50, -1], [1, 25, 50, "All"]],
                                 "data" : data,
                                 "columns":cols                     
          		 };
           	           
				var table = $('#table_update_status').DataTable(opt);	
            }
        });	
	}
    //if ($(".nav li:eq(1)").hasClass("active")) {
    if (filename == "main.php") {		
    	active = 1;
    	loadLevelData(1, 0);
		$("#add_btn").click(function() {
			$("#table_list_wrapper").hide();
			$("#table_add").show();
			$("#add_btn").hide();
			$("#return_btn").show();
			$("#add_row").show();
			appendAddRow(0);
		})
		$("#return_btn").click(function() {
			$("#table_list_wrapper").show();
			$("#table_add").hide();
			$("#add_btn").show();
			$("#return_btn").hide();
			$("#add_row").hide();
		})


		$("#add_append_btn").click(function() {
			appendAddRow(1);
		})

		$("#form_add_level").validate({
			rules: {
				"voc_name[]": {
					unique: true
					/*
					unique: true,
					remote: {
						url: "api.php",
						type: "post",
						data: {
						  voc_names: function() {
						    return $( "input[name='voc_name[]']" ).map(function() {return this.value;}).get().join();
						    //return $('input[name="voc_name[]"]').serialize();
						  },
						  act: "chkVocExist"
						  
						},
		                dataFilter: function(data,type) {
		                	alert("datafilter="+data);
		                    if(data == "true") {
		                    	validator.errorList.length = 0;
		                        return true;
		                    } else {
		                    	return false;
		                    	var json = JSON.parse(data);
		                		$.each(json, function(i, item) {
								    if (item == "Y") {
								    	$("#table_add input[name='voc_name[]']:eq(" + i + ")").after( ' <span class="glyphicon glyphicon-remove-sign" title="名稱已存在" style="color:red"></span>');
								    }
								})			                    			                    	
		                    	return "\"輸入名稱已存在!\"";
		                    	return false;
		                    }
		                   
		                }
					}
					*/					
				}	
			}, 
			messages: {
				"voc_name[]":{
					unique: "名稱重覆!"
					/*
					unique: "名稱重覆!",
					remote: "輸入名稱已存在!"
					*/
				}		
			}, 
			onkeyup: false,
			onfocusout: false,
			showErrors: function(errorMap, errorList) {  
				/*
				var msg = "";  
				$.each( errorList, function(i,v){  
				 msg += (v.message+"\r\n");  
				});  
				if(msg!="") alert(msg);  
				*/
			},  		
			invalidHandler: function(form, validator) {
				var errors = validator.numberOfInvalids();
				if (errors) {
				
					var errors = "";
					if (validator.errorList.length > 0) {
						for (x=0;x<validator.errorList.length;x++) {
							errors += "\n\u25CF " + validator.errorList[x].message;
						}
					}
					alert(errors);
				}
				validator.focusInvalid();
			},
		    submitHandler: function(form) {
				var level = $("select.selectpicker:last").data("level");
				var pid = $("select.selectpicker:last option:selected").val();
				if (level == 5 && pid != "") {
					level = 6;
				}
				if (pid == "") {
					var last_idx = $(".dropdown-area select.selectpicker").length - 2;
					pid =  $("select.selectpicker:eq(" + last_idx + ") option:selected").val();
				}
				if (level == 1 && pid == "") {
					pid = 0;
				}		    	
		        $.ajax({
		            url: "api.php",
		            type: "post",
		            data: $(form).serialize()+'&act=chkVocExist2&level=' + level + '&pid=' + pid,
		            success: function(response) {
		            	$("#table_add span.glyphicon-remove-sign").remove();
	            		if (response != "true") {
	                    	var json = JSON.parse(response);
	                		$.each(json, function(i, item) {
							    if (item == "Y") {
							    	$("#table_add input[name='voc_name[]']:eq(" + i + ")").after( ' <span class="glyphicon glyphicon-remove-sign" title="名稱已存在" style="color:red"></span>');
							    }
							})
							alert("輸入名稱已存在!");		            			
	            		} else {

					        $.ajax({
					            url: "api.php",
					            type: "post",
					            data: $(form).serialize()+'&act=insertVoc&level=' + level + '&pid=' + pid,
					            success: function(response) {
					            	if (response == "OK") {
					            		//$("#table_list").DataTable().destroy();
										$("#table_list_wrapper").show();
										$("#table_add").hide();
										$("#add_btn").show();
										$("#return_btn").hide();
										$("#add_row").hide();
										$("input[name^=voc_name]").val('');	
										loadLevelData(level, pid);	            		
					            	} else {
					            		alert(response);
					            	}
					               
					            }            
					        });	            			
	            		}
		            }            
		        });		    	


		    }		
		});	

		$(".dropdown-area").on("change", "select.dropdown-level", function() {
			changeLevelDropdown($(this).data("level"), $(this).val());
		})
		/*
		$("#table_add").on("keypress", "input[name='voc_value[]']" ,function(e) {
			event = e || window.event;
			var charCode = (typeof event.which == "number") ? event.which : event.keyCode;
			
    		//if(charCode < 48 || charCode > 57){
    		if ((charCode >= 48 && charCode <= 57) || (charCode >= 96 && charCode <= 105) || (charCode == 8)) { 	
    			return true;
    		} else {
    			return false;
    		}
		});
		*/
	}

	//if ($(".nav li:eq(2)").hasClass("active")) {
	if (filename == "export.php" || filename =="project.php") {		
		active = 2;
		$("#table_import").show();
		loadLevelData(1, 0);
		if (pro_id == "") {
			$("#btn_delete").parent("li").hide();
		}
		$(".dropdown-area").on("change", "select.dropdown-level", function() {
			$('#save_public_project_btn').hide();
			if ($(this).data("level") == 1) {
				changeLevelDropdown($(this).data("level"), $(this).val());
				loadimportData(-1);
			} else if ($(this).data("level") == 2 && $(this).val() != "") {
				loadimportData($(this).val());
				if (filename == "project.php") {
					$('#save_public_project_btn').show();
				}
			}	
		})		
    	$("#toggle_arrow").click(function() {
    		$("#table_div").slideToggle('slow');
    		if ($(this).hasClass("glyphicon-chevron-up")) {
    			$(this).removeClass("glyphicon-chevron-up").addClass("glyphicon-chevron-down");
    		} else {
    			$(this).removeClass("glyphicon-chevron-down").addClass("glyphicon-chevron-up");
    		}
    	})
		var button='<button class="close" type="button" title="Remove this page">×</button>';
		var tabID = 1;
		function resetTab(){
			var tabs=$("#tab-list li:not(:first)");
			var len=1
			$(tabs).each(function(k,v){
				len++;
				$(this).find('a').html('Tab ' + len + button);
			})
			tabID--;
		}

	    $('#btn-add-tab').click(function() {
	        tabID++;
	        $('#tab-list').append($('<li><a href="#tab' + tabID + '" role="tab" data-toggle="tab"><span>Tab ' + tabID + '</span> <span class="glyphicon glyphicon-pencil text-muted edit"></span> <button class="close" type="button" title="Remove this page">×</button></a></li>'));
	        $('#tab-content').append($('<div class="tab-pane fade in" id="tab' + tabID + '"><div class="row"><div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 text-right"><select name="lv4[]" id="lv4_' + tabID + '" multiple size="10"></select><button id="btn_remove_4_' + tabID + '" class="btn btn-danger btn-remove-4"><span class="glyphicon glyphicon-remove"></span></button></div><div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 text-center"><button id="btn_send_' + tabID + '" class="btn btn-info btn-send"><span class="glyphicon glyphicon-chevron-right"></span></button></div><div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="width:250px"><select name="lv4s[]" id="lv4s_' + tabID + '" multiple size="10"></select><button id="btn_remove_4s_' + tabID + '" class="btn btn-danger btn-remove-4s"><span class="glyphicon glyphicon-remove"></span></button></div></div></div></div>'));
	        $(".edit").click(editHandler);
	    });
	    
	    $('#tab-list').on('click', '.close', function() {
	    	if (confirm('確定刪除此頁籤(包含選取項目)?')) {
		        var tabID = $(this).parents('a').attr('href');
		        $(this).parents('li').remove();
		        $(tabID).remove();

		        //display first tab
		        var tabFirst = $('#tab-list a:first');
		        resetTab();
		        tabFirst.tab('show');
	    	}
	    });

	    var list = document.getElementById("tab-list");

		var editHandler = function() {
		  var t = $(this);
		  t.css("visibility", "hidden");
		  $(this).prev().attr("contenteditable", "true").focusout(function() {
		    $(this).removeAttr("contenteditable").off("focusout");
		    t.css("visibility", "visible");
		  });
		};

		$(".edit").click(editHandler);
		//$("button.btn-send").click(function() {
		$("#tab-content").on("click", "button.btn-send", function() {	
			var lv4_ids = $(this).closest(".tab-pane").find("select[name='lv4[]']").val();
			var lv4_pool = $(this).closest(".tab-pane").find("select[name='lv4[]']");
			var lv4_select = $(this).closest(".tab-pane").find("select[name='lv4s[]']");
			lv4_pool.children("option:selected").each(function() {
				if (lv4_select.find("option[value='"+$(this).val()+"']").length == 0) {
					lv4_select.append('<option value="'+$(this).val()+'">'+$(this).text()+'<span class="glyphicon glyphicon-remove"></span></option>');
				}				
			})
			/*
	        $.ajax({
	            url: "api.php",
	            type: "post",
	            data: {
	            		act : "getLevelDataArr",
	            		ids : lv4_ids
	            },
	            success: function(response) {
	            	var data = JSON.parse(response);
					$.each(data, function(index, item) {
						$(".tab-pane.active").children("select[name^=lv4s]").append("<option value='"+item.voc_id+"'>"+item.voc_name+"</option>");
					});	            		           
	            }            
	        });
	        */		
		})
		$("#tab-content").on("click", "button.btn-remove-4", function() {
			var lv4_pool = $(this).parents("div.tab-pane").find("select[name='lv4[]']");
			lv4_pool.children("option:selected").remove();
		}).on("click", "button.btn-remove-4s", function() {
			var lv4_select = $(this).parents("div.tab-pane").find("select[name='lv4s[]']");
			lv4_select.children("option:selected").remove();
		});	
		$("body").on("dblclick", "select[name^=lv4] option", function() {
			//alert($(this).val());
		})
		$("#form_project").validate({
			rules: {
				"pro_name": {
					required: true,
					remote: {
						url: "api.php",
						type: "post",
						data: {
						  pro_name: function() {
						    return $("input[name='pro_name']").val();
						  },
						  act: "chkProjectExist"
						}
					}					
				}									
			}, 
			messages: {
				"pro_name":{
					required: "請輸入專案名稱!",
					remote: "專案名稱已存在!"
				}														
			}, 
			onkeyup: false,
			onfocusout: false,
			showErrors: function(errorMap, errorList) {  
				/*
				var msg = "";  
				$.each( errorList, function(i,v){  
				 msg += (v.message+"\r\n");  
				});  
				if(msg!="") alert(msg);  
				*/
			},  		
			invalidHandler: function(form, validator) {
				var errors = validator.numberOfInvalids();
				if (errors) {
				
					var errors = "";
					if (validator.errorList.length > 0) {
						for (x=0;x<validator.errorList.length;x++) {
							errors += "\n\u25CF " + validator.errorList[x].message;
						}
					}
					alert(errors);
				}
				validator.focusInvalid();
			},
		    submitHandler: function(form) {
				insertProject();
		    }		
		});
		$("#btn_save").click(function() {
			if (pro_name == "") {
				$("#saveModal").modal("show");
			} else {
				insertProject('save');
			}
		})
		$("#btn_saveas").click(function() {
			if (pro_name != "") {
				//$("#pro_name").val(pro_name);
			}
		})		
		$("#btn_load_project").click(function() {
	        $.ajax({
	            url: "api.php",
	            type: "post",
	            data: {
	            	act : "getProject"
	            },
	            dataType:"json",
	            success: function(response) {
	            	if (response.length == 0) {
	            		$("#dropdown_project").selectpicker("hide");
	            		$("#btn_load").hide();
	            		$("#project_message").show();
	            	} else {
	            		$("#dropdown_project").selectpicker("show");
	            		$("#btn_load").show();
	            		$("#project_message").hide();
	            	}
	            	$("#dropdown_project").empty();
	            	$.each(response, function(index, item) {
	            		$("#dropdown_project").append("<option value='"+item.pro_id+"'>"+item.pro_name+"</option>");
	            	});
	            	$("#dropdown_project").selectpicker('refresh');
	            }            
	        });			
		})
		$("#btn_load").click(function() {
			pro_id = $("#dropdown_project").val();
			pro_name = $("#dropdown_project option:selected").text();
			$("#btn_delete").parent("li").show();
			$("#project_display_name").text(pro_name);
	        $.ajax({
	            url: "api.php",
	            type: "post",
	            data: {
	            	act : "loadProject", 
	            	pro_id : $("#dropdown_project").val()
	            },
	            dataType:"json",
	            success: function(response) {
	            	console.log(response);
	            	$("#tab-list li").remove();
	            	$("#tab-content div").remove();
	            	tabID = 0;
	            	$.each(response, function(index, item) {
	        			tabID++;
	        			if (tabID == 1) {
	        				$('#tab-list').append($('<li><a href="#tab' + tabID + '" role="tab" data-toggle="tab"><span>' + item.tab_name + '</span> <span class="glyphicon glyphicon-pencil text-muted edit"></span></a></li>'));
	        			} else {
	        				$('#tab-list').append($('<li><a href="#tab' + tabID + '" role="tab" data-toggle="tab"><span>' + item.tab_name + '</span> <span class="glyphicon glyphicon-pencil text-muted edit"></span> <button class="close" type="button" title="Remove this page">×</button></a></li>'));
	        			}
	        			
	        			$('#tab-content').append($('<div class="tab-pane fade in" id="tab' + tabID + '"><div class="row"><div class="col-lg-5 col-md-5 col-sm-5 col-xs-5 text-right"><select name="lv4[]" id="lv4_' + tabID + '" multiple size="10"></select><button id="btn_remove_4_' + tabID + '" class="btn btn-danger btn-remove-4"><span class="glyphicon glyphicon-remove"></span></button></div><div class="col-lg-2 col-md-2 col-sm-2 col-xs-2 text-center"><button id="btn_send_' + tabID + '" class="btn btn-info btn-send"><span class="glyphicon glyphicon-chevron-right"></span></button></div><div class="col-lg-5 col-md-5 col-sm-5 col-xs-5" style="width:250px"><select name="lv4s[]" id="lv4s_' + tabID + '" multiple size="10"></select><button id="btn_remove_4s_' + tabID + '" class="btn btn-danger btn-remove-4s"><span class="glyphicon glyphicon-remove"></span></button></div></div></div></div>'));
	        			var lv4 = item.lv4;
	        			$.each(lv4, function(index_lv4, item_lv4) {
	        				$("#lv4_"+tabID).append("<option value='"+item_lv4.voc_id+"'>"+item_lv4.voc_name+"</option>");
	        			});
	        			var lv4s = item.lv4s;
	        			$.each(lv4s, function(index_lv4s, item_lv4s) {
	        				$("#lv4s_"+tabID).append("<option value='"+item_lv4s.voc_id+"'>"+item_lv4s.voc_name+"</option>");
	        			});	        			
	            	});
	            	$('#tab-list a:first').tab("show");
	            	$("#loadModal").modal("hide");
	            	$("#pro_name").val(pro_name);
	            	$(".edit").click(editHandler);
	            }            
	        });				
		})
		$("#btn-tab-export").click(function() {
			if ($("#tab-content select[name='lv4s[]'] option").length > 0) {
				var lv4_ids = '';
				$.each($("#tab-content select[name='lv4s[]'] option"), function(index, item) {
					lv4_ids += item.value + ',';
				});
				$.ajax({
		            url: "api.php",
		            type: "post",
		            data: {
		            	act : "chkExportTab", 
		            	lv4_ids : lv4_ids
		            },
		            dataType:"json",
		            success: function(response) {
		            	if (response > 0) {
		            		//location.href = "export_tab.php?lv4_ids=" + lv4_ids + "&lv4s_ids=&tab_name=" + pro_name;
		            		var url = 'export_tab.php';
							var form = $('<form action="' + url + '" method="post">' +
							  '<input type="text" name="data" value=\'' + JSON.stringify(lv4_ids) + '\' />' +
							  '<input type="text" name="tab_name" value=\'' + pro_name + '\' />' +
							  '</form>');
							$('body').append(form);
							form.hide();
							form.submit();
							form.remove();
		            	} else {
		            		alert('無符合條件詞彙需匯出!');
		            	}
					}            
	        	});					
			} else {
				alert('無符合條件詞彙需匯出!');
			}
		})
		$("#project_export").click(function() {
			var data = new Object();
			if (pro_name == "") {
				data.pro_name = "";
			} else {
				data.pro_name = pro_name;
			}
			var project_data_array = [];
	    	$.each($("#tab-list li"), function(index, item) {
	    		var tab_data = {};
	    		var tab_data_arr = [];
	    		$.each($("select[name='lv4s[]']:eq("+ index +") option"), function(k, v) {
	    			tab_data_arr.push($(this).val());
	    		})
	    		tab_data["tab_name"] = $(this).find("span:first").text();
	    		tab_data["data"] = tab_data_arr;
	    		project_data_array.push(tab_data);	
	    	});
	    	data.data = project_data_array;	
	    	//console.log(JSON.stringify(data));		
			//location.href = "export_project.php?data=" + JSON.stringify(data);
			//$.post( "export_project.php", "data=" + JSON.stringify(data));
			var url = 'export_project.php';
			var form = $('<form action="' + url + '" method="post">' +
			  '<input type="text" name="data" value=\'' + JSON.stringify(data) + '\' />' +
			  '</form>');
			$('body').append(form);
			form.hide();
			form.submit();
			form.remove();
		});
		$('#keyword').keypress(function (e) {
		  if (e.which == 13) {
		    $("#btn_search").trigger("click");
		    //return false;    //<---- Add this line
		  }
		});		
		$("#btn_search").click(function() {
			if ($("#keyword").val() != "") {
		        $.ajax({
		            url: "api.php",
		            type: "post",
		            data: {
		            	act : "searchVoc",
		            	keyword : $("#keyword").val()
		            },
		            success: function(response) {
		            	console.log(response);
		            	if (response == "false") {
		            		alert("無此資料!");
		            		$("#keyword").val("");
		            	} else {
		            		data = JSON.parse(response);
		            		var id1 = data["id1"];
		            		var id2 = data["id2"];
		            		var id3 = data["id3"];
		            		$("#dropdown_lv1").val(id1).selectpicker("refresh");
		            		loadLevelData(2, id1);
		            		if (id2 > 0) {
		            			setTimeout(function(){
		            				$("#dropdown_lv2").val(id2).selectpicker("refresh");
		            			}, 200);
		            			loadimportData(id2);	
		            		}
		            		if (id3 > 0) {
		            			loadimportData(id2);
		            		}
		            	}
		            	//console.log(response);
		            }            
		        });				
			}		
		})
		$("#tab-list").on("click", "li", function() {
			setTimeout(function(){
				if ($("#tab-content div.active select[name='lv4[]'] option").length > 0) {
					var lv4 = [];
					$.each($("#tab-content div.active select[name='lv4[]'] option"), function(index, item) {
						lv4.push(item.value);
					});
			        $.ajax({
			            url: "api.php",
			            type: "post",
			            data: {
			            	act : "chkLV4Exist",
			            	lv4 : lv4
			            },
			            dataType : "json",
			            success: function(response) {
			            	$.each(response, function(index, item) {
			            		if($("#tab-content div.active select[name='lv4[]'] option:eq("+index+")").val() != item) {
			            			$("#tab-content div.active select[name='lv4[]'] option:eq("+index+")").addClass("text-danger");
			            		}
			            	})
			            }            
			        });	
				}
			}, 200);

		})
		$("#btn_delete").click(function() {
			if (confirm("確定刪除[" + pro_name + "]專案?")) {
		        $.ajax({
		            url: "api.php",
		            type: "post",
		            data: {
		            	act : "deleteProject",
		            	pro_id : pro_id
		            },
		            dataType : "json",
		            success: function(response) {
		            	if (response.msg){
		            		alert("刪除成功!");
		            		location.reload();
		            	} else {
		            		alert(response.msg);
		            	}
		            }            
		        });					
			}
		})		
	}
	//if ($(".nav li:eq(3)").hasClass("active")) {
	if (filename == "import.php") {		
		active = 3;
		$("#import_message").hide();
		loadLevelData(1, 0);
		$(".dropdown-area").on("change", "select.dropdown-level", function() {
			$("#import_message").hide();
			if ($(this).data("level") == 1) {
				changeLevelDropdown($(this).data("level"), $(this).val());
				$("#table_import").DataTable().destroy();  
				$("#table_import").hide();
				$("#table_import_confirm").DataTable().destroy();  
				$("#table_import_confirm").hide();
				$('#btn_import').hide();

			} else if ($(this).data("level") == 2 && $(this).val() != "") {
				$("#dropdown_lv3").selectpicker("destroy").remove();
				loadimportData($(this).val());
				$("#table_import").show();
				$("#table_import_confirm").DataTable().destroy();  
				$("#table_import_confirm").hide();
				$('#btn_import').hide();
			} else if ($(this).data("level") == 3) {
				$("#dropdown_lv3").selectpicker("destroy").remove();
				loadimportData($("#dropdown_lv2").val());
				$("#table_import").show();
				$("#table_import_confirm").DataTable().destroy();  
				$("#table_import_confirm").hide();
				$('#btn_import').hide();				
			}	
		})
		$("#form_upload").validate({
			rules: {
				"upload_file": {
					required: true,
					extension: "xls|xlsx|csv"
				}						
			}, 
			messages: {
				"upload_file":{
					required: "請選擇匯入檔案!",
					extension: "請選擇EXCEL格式檔案!"
				}													
			}, 
			onkeyup: false,
			onfocusout: false,
			showErrors: function(errorMap, errorList) {  
				/*
				var msg = "";  
				$.each( errorList, function(i,v){  
				 msg += (v.message+"\r\n");  
				});  
				if(msg!="") alert(msg);  
				*/
			},  		
			invalidHandler: function(form, validator) {
				var errors = validator.numberOfInvalids();
				if (errors) {
				
					var errors = "";
					if (validator.errorList.length > 0) {
						for (x=0;x<validator.errorList.length;x++) {
							errors += "\n\u25CF " + validator.errorList[x].message;
						}
					}
					alert(errors);
				}
				validator.focusInvalid();
			},
		    submitHandler: function(form) {
		    	var voc_id1 = $("#voc_id1").val();
		    	var voc_id2 = $("#voc_id2").val();
		    	var voc_id3 = $("#voc_id3").val();
			    var file_data = $('#upload_file').prop('files')[0];   
			    var form_data = new FormData();                  
			    form_data.append('spreadsheet', file_data);
			    form_data.append('voc_id1', voc_id1);
			    form_data.append('voc_id2', voc_id2);
			    form_data.append('voc_id3', voc_id3);
			    /*
			    var xhr = new XMLHttpRequest();
				xhr.open('POST', "upload.php", true);
				xhr.onload = function(e) { 
				    if (this.status == 200) {
				      console.log(this.responseText);
				    }
				};

				xhr.send(form_data);
				*/
		        $.ajax({
		            url: "upload.php",
		            type: "post",
	                dataType: 'text',  // what to expect back from the PHP script, if anything
	                cache: false,
	                contentType: false,
	                processData: false,		            
		            data: form_data,
		            success: function(response) {
		            	$("#table_import").DataTable().destroy();
		            	$("#table_import").hide();
		               	$('#uploadModal').modal('toggle');
		               	$(".dropdown-area").append(' <select class="selectpicker dropdown-level" data-style="btn-mute" id="dropdown_lv3" data-level="3"><option value="">--請選擇--</option></select>'); 
						$.ajax({
							url:'api.php',
							data: {
								act:'getLevelData',
								lv:3,
								pid: voc_id2																
							},
							type:"POST",
							dataType:"json",
							success: function(data, textStatus, jqXHR) {

								$.each(data, function(index, item) {
									item.no = index+1;
								    $('#dropdown_lv3').append($('<option/>', { 
								        value: item.voc_id,
								        text : item.voc_name 
								    })).selectpicker('refresh');
								    $('#dropdown_lv3').val(voc_id3);
								    $('#dropdown_lv3').selectpicker('refresh');
								})
							},
							error: function(jqXHR, textStatus, errorThrown) {
								console.log(errorThrown);	
							}
						});
						//remove bom
                        var data = JSON.parse(response.replace(/^[\s\uFEFF\xa0\u3000]+|[\uFEFF\xa0\u3000\s]+$/g, ""));
						$("#chk_all").attr("disabled", false);
						$("#table_import_confirm").show();
						$('#btn_import').show();
						console.log(data);	
						$.each(data, function(index, item) {
							item.no = index+1;
							item.voc_name4 = item[0][0];
							item.voc_name5 = item[0][1];
							item.voc_name6 = item[0][2];
							item.voc_value = item[4];
							item.voc_new_value = item[0][3];
							item.msg = item[5];
							item.voc_id4 = item[1];
							item.voc_id5 = item[2];
							item.voc_id6 = item[3];							
						})
						cols = [
				        {"data": "no"},
				        {"data": "voc_name4"},
				        {"data": "voc_name5"},
				        {"data": "voc_name6"},
				        {"data": "voc_value"},
				        {"data": "voc_new_value"},
				        //{"data": "msg"},
				        {"data": "voc_id4"},
				        {"data": "voc_id5"},
				        {"data": "voc_id6"}
				   		 ];	     			           	
						var opt={"oLanguage":{"sProcessing":"處理中...",
	                         "sLengthMenu":"顯示 _MENU_ 筆資料",
	                         "sZeroRecords":"沒有匹配結果",
	                         "sInfo":"顯示第 _START_ 至 _END_ 項結果，共 _TOTAL_ 項",
	                         "sInfoEmpty":"顯示第 0 至 0 筆資料，共 0 項",
	                         "sInfoFiltered":"(從 _MAX_ 項資料過濾)",
	                         "sSearch":"搜索:",
	                         "oPaginate":{"sFirst":"首頁",
	                                              "sPrevious":"上頁",
	                                              "sNext":"下頁",
	                                              "sLast":"尾頁"}
	                         },
	                         "processing": true,
	                         "bSort": false,
	                         "searching": false,
	                         "lengthChange": false,
	                         "bPaginate": false,
	                         //"lengthMenu": [[1, 25, 50, -1], [1, 25, 50, "All"]],
	                         "data" : data,
	                         "columns":cols,
					        "columnDefs": [ {
					            "targets": 9,
					            "data": null,
					            "defaultContent": "<input type='checkbox' name='status'>"
					        },{
					            "targets": [6,7,8],
					            "visible": false
					        }]                       
		          		 };
		           	           
						var table = $('#table_import_confirm').DataTable(opt);
						$('#table_import_confirm').show();
						$('#table_import_confirm td').css('text-align', 'center');
						$('#btn_import').show();
						
						$("#btn_import").click(function() {
							var stat = new Array();
							$.each($("input:checkbox[name='status']"), function(index, item) {
								if ($("input:checkbox[name='status']:eq("+index+")").prop("checked")) {
									stat.push("Y");
								} else {
									stat.push("N");
								}
							});
							//console.log(stat);
							var import_data = new Array();
							if ($("input:checkbox[name='status']:checked").length > 0) {
								table.rows().every( function () {
								    var d = this.data();
								 
								    d.counter++; // update data source for the row
								 
								    this.invalidate(); // invalidate the data DataTables has cached for this row
								    var index = this.index();
								    if (stat[index] == "Y" && d.msg == "") {
								    	var rowdata = new Array();
								    	rowdata.push({id3: $("#dropdown_lv3").val(), id4: d.voc_id4, id5: d.voc_id5, id6: d.voc_id6, name4: d.voc_name4, name5: d.voc_name5, name6: d.voc_name6, voc_value: d.voc_new_value });
								    	import_data.push(rowdata);
								    	rowdata = [];
								    }
								} );
								console.log(import_data);
								stat = [];
								if (import_data.length > 0) {
							        $.ajax({
							            url: "api.php",
							            type: "post",
							            data: {act:"importVoc", 
							            import_data : JSON.stringify(import_data)},
							            success: function(response) {
							            	$('#table_import_confirm').DataTable().destroy();
							            	$('#table_import_confirm').hide();
							            	$('#chk_all').prop('checked', false);
							            	$("#btn_import").hide();
							            	$('#import_message div').empty().append(response);
							            	$('#import_message').show();
							            }            
							        });										
									}

						        import_data = [];	
						       	table.clear();					
							}

						})		
		            },
		            error: function (jqXHR, exception) {
            			alert(jqXHR.responseText);
            		}
		        });

		    }		
		});
		$('#chk_all').click(function(event) {   
		    if(this.checked) {
		        // Iterate each checkbox
		        $(':checkbox').each(function() {
		            this.checked = true;                        
		        });
		    } else {
		        $(':checkbox').each(function() {
		            this.checked = false;                        
		        });		    	
		    }
		});

	}	

	//if ($(".nav li:eq(4)").hasClass("active")) {
	if (filename == "user.php") {		
		active = 4;
		$("#add_btn").click(function() {
			$("#usr_account").attr("readonly", false);
			$("#form_user input").val('');
			$(".modal-title").text("新增帳號");
		})

		loadUserData();
		$("#form_user").validate({
			rules: {
				"usr_account": {
					required: true,
					remote: {
						url: "api.php",
						type: "post",
						data: {
						  usr_account: function() {
						    return $( "input[name='usr_account']" );
						  },
						  act: "chkUserAccountExist"
						}
					}					
				}, 
				"usr_password" : {
					required: true
				},
				"usr_name" : {
					required: true
				},
				"usr_type" : {
					required: true
				}									
			}, 
			messages: {
				"usr_account":{
					required: "請輸入帳號!",
					remote: "帳號已存在!"
				},
				"usr_password":{
					required: "請輸入密碼!"
				},
				"usr_name":{
					required: "請輸入姓名!"
				},
				"usr_type":{
					required: "請選擇使用權限!"
				}														
			}, 
			onkeyup: false,
			onfocusout: false,
			showErrors: function(errorMap, errorList) {  
				/*
				var msg = "";  
				$.each( errorList, function(i,v){  
				 msg += (v.message+"\r\n");  
				});  
				if(msg!="") alert(msg);  
				*/
			},  		
			invalidHandler: function(form, validator) {
				var errors = validator.numberOfInvalids();
				if (errors) {
				
					var errors = "";
					if (validator.errorList.length > 0) {
						for (x=0;x<validator.errorList.length;x++) {
							errors += "\n\u25CF " + validator.errorList[x].message;
						}
					}
					alert(errors);
				}
				validator.focusInvalid();
			},
		    submitHandler: function(form) {
		    	var act = "insertUser";
		    	if ($('#usr_id').val() > 0) {
		    		act = "updateUser";
		    	}
		        $.ajax({
		            url: "api.php",
		            type: "post",
		            data: $(form).serialize()+'&act='+act,
		            success: function(response) {
		            	if (response == "OK") {
		            		loadUserData();
		            		$("#form_user input").val('');
		            		$("#usr_type option:first").attr('selected', true);
		            		$("#usr_type").selectpicker('refresh');
							$('#editModal').modal('toggle');
							$('#usr_account').attr('readonly', false);
		            	} else {
		            		alert(response);
		            	}
		               
		            }            
		        });

		    }		
		});	        	
	}

	if (filename == "project.php") {
		$('#save_public_project_btn').hide();

		$('#tab_admin_project').click(function() {
			$("#table_project tbody").empty();
			$.ajax({
	            url: "api.php",
	            type: "post",
	            data: {
	            	act : "getProject"
	            },
	            dataType:"json",
	            success: function(response) {
	            	$.each(response, function(index, item) {
	            		var no = index + 1;
	            		var pro_type = '私有';
	            		if (item.pro_type == 1) pro_type = '公版';
	            		$("#table_project tbody").append("<tr><td class='text-center'>" + no + "</td><td><a href='javascript:;' class='pro_name' data-id='" + item.pro_id + "' id='pro_name_" + item.pro_id + "'>" + item.pro_name + "</a></td><td class='text-center'>" + 
	            		item.pro_create_time.substring(0,19) + "</td><td class='text-center'>" + item.pro_update_time.substring(0,19) + "</td><td>" + 
	            		item.pro_id + "</td><td class='text-center'>" + pro_type + "</td><td class='text-center'><button class='btn btn-danger btn_delete_project' data-id='" + item.pro_id + "'>刪除</button> <button class='btn btn-info btn_export_project' data-id='" + item.pro_id + "'>匯出</button></td></tr>");
	            	});
	            }            
	        });
		})

		$('#table_project').on('click', '.pro_name', function() {
			var title = $(this).text();
			var pro_id = $(this).data('id');
			$.ajax({
	            url: "api.php",
	            type: "post",
	            data: {
	            	act : "loadProject", 
	            	pro_id : $(this).data('id')
	            },
	            dataType:"json",
	            success: function(response) {

	            	$('#panel_project_detail').show();
	            	$('#panel_project_detail').attr('data-id', pro_id);
	            	$('#project_title').text(title);
	            	$('#table_project_detail tbody').empty();
	            	$('#project_group').empty().append('<option value="">群組名稱(全選)</option>');
	            	var no = 0;
	            	$.each(response, function(index, item) {

	        			var lv4s = item.lv4s;
	        			$('#project_group').append('<option value="' + item.tag_id + '">' + item.tab_name + '</option>');
	        			$.each(lv4s, function(index_lv4s, item_lv4s) {
	        				no++;
	        				$("#table_project_detail tbody").append("<tr data-tag-id='" + item.tag_id + "'><td class='text-center'><input type='checkbox' class='chk_glossary' data-id='" + item_lv4s.glo_id + "'></td><td>" + no + "</td><td>" + item.tab_name + "</td><td>" + item_lv4s.voc_name + "</td></tr>");
	        			});	        			
	            	});
	            	$('#project_group').selectpicker('refresh');
	            }            
	        });	
		})
		
		$('#chk_all_project_detail').click(function() {
			var stat = $(this).prop('checked');
			$('#table_project_detail input[type=checkbox]').prop('checked', stat);
		})

		$('#panel_project_detail').on('change', '#project_group', function() {
			var tag_id = $(this).val();
			if (tag_id == '') {
				$('#table_project_detail tbody tr').show();
			} else {
				$.each($('#table_project_detail tbody tr'), function(index, item) {
					if ($(this).data('tag-id') == tag_id) {
						$(this).show();
					} else {
						$(this).hide();
					}
				})	
			}
			$('#table_project_detail input[type=checkbox]').prop('checked', false);
		})

		$('#btn_del_glossary').click(function() {
			var pro_id = $('#panel_project_detail').attr('data-id');
			if ($('.chk_glossary:checked').length > 0) {
				if (confirm('確定刪除' + $('.chk_glossary:checked').length + '筆辭彙?')) {
					var glossary_ids = new Array();
					$.each($('.chk_glossary:checked'), function() {
						glossary_ids.push($(this).data('id'));
					})
					$.ajax({
			            url: "api.php",
			            type: "post",
			            data:'act=deleteGlossary&glossary_ids=' + JSON.stringify(glossary_ids),
			            success: function(response) {
			            	if (response == "OK") {
			            		alert("刪除成功!");
			            		$('#pro_name_' + pro_id).trigger('click');
			            	} else {
			            		alert(response);
			            	}
			               
			            }            
			        });			
				}	
			} else {
				alert('請選擇要刪除的辭彙!');
			}
			
		})

		$('#table_project').on('click', '.btn_delete_project',  function() {
			var pro_name = $(this).parents('tr').find('.pro_name').text();
			if (confirm('確定刪除[' + pro_name + ']專案?')) {
 				$.ajax({
		            url: "api.php",
		            type: "post",
		            data: {
		            	act : "deleteProject",
		            	pro_id : $(this).data('id')
		            },
		            dataType : "json",
		            success: function(response) {
		            	if (response.msg){
		            		$('#panel_project_detail').hide();
		            		$('#tab_admin_project').trigger('click');
		            		alert("刪除成功!");
		            	} else {
		            		alert(response.msg);
		            	}
		            }            
		        });			
			}
		})

		$('#table_project').on('click', '.btn_export_project', function() {
			var data = $(this).data('id');
			var url = 'export_project_all.php';
			var form = $('<form action="' + url + '" method="post">' +
			  '<input type="text" name="data" value=\'' + JSON.stringify(data) + '\' />' +
			  '</form>');
			$('body').append(form);
			form.hide();
			form.submit();
			form.remove();
			console.log($(this).data('id'));
		})
		$('#save_public_project_btn').click(function() {
			var $pro_name = $('#dropdown_lv2 option:selected').text();
			var $pro_lv2_id = $('#dropdown_lv2').val();
			if (confirm('確認儲存[' + $pro_name + ']公版專案？')) {
				$.ajax({
					url: "api.php",
					type: "post",
					data: {
						act : "chkProjectExist",
						pro_name : $pro_name
					},
					success: function(response) {
						console.log(response);
						if (response == 'false') {
							alert("專案[" + $pro_name + "]已存在");
						} else {
							var project_data = {};

							project_data["pro_name"] = $pro_name;
							project_data['pro_lv2_id'] = $pro_lv2_id;
							console.log(project_data);
							$.ajax({
								url: "api.php",
								type: "post",
								data: {
									act : "insertPublicProject",
									project_data : JSON.stringify(project_data)
								},
								success: function(response) {
									console.log(response);
									data = JSON.parse(response);
									if (data.pro_id != '') {
										pro_id = data.pro_id;
										pro_name = data.pro_name;
										alert("公版專案[" + pro_name + "]儲存成功");
									} else {
										alert(response);
									}

								}
							});
						}

					}
				});


			}
		})
	}
})

function GetURLParameter(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) 
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) 
        {
            return sParameterName[1];
        }
    }
}

function appendAddRow(flag) {
	if (flag == 0) {
		$("#table_add tbody").empty();
	}
	
	var row_count = $("#table_add tbody tr").length;
	
	for (i = 1; i <= 5; i++) {
		var no = row_count + i;
		if ($("select.dropdown-level").length == 5 && $("select.dropdown-level:last option:selected").val() != "") {
			$("#table_add tbody").append("<tr><td>" + no + "</td><td><input type='text' name='voc_name[]' class='unique'></td><td><input type='text' name='voc_value[]' value=0   onInput ='keypress(this)' ></td></tr>"); 
			$("#table_add thead th:eq(2)").show();
		} else {			
			$("#table_add tbody").append("<tr><td>" + no + "</td><td><input type='text' name='voc_name[]' class='unique'></td></td></tr>"); 
			$("#table_add thead th:eq(2)").hide();
		}	
	}
}

function loadLevelData(level, pid) {
	$.ajax({
		url:'api.php',
		data: {
			act:'getLevelData',
			lv:level,
			pid: pid																
		},
		type:"POST",
		dataType:"json",
		success: function(data, textStatus, jqXHR) {

			if (level < 6) {
				$('#dropdown_lv'+level).empty().append('<option value="">--請選擇' + WORD_TYPE[level-1] + '--</option>');
			}
			
			if (data.length == 0) {
				$('#dropdown_lv'+level).selectpicker('refresh');
			} else {
				//console.log(data);
				$.each(data, function(index, item) {
					item.no = index+1;
				    $('#dropdown_lv'+level).append($('<option/>', { 
				        value: item.voc_id,
				        text : htmlDecode(item.voc_name) 
				    })).selectpicker('refresh');
				    
				})				
			}

			if (active == 1) {
				cols = [
		        {"data": "no"},
		        {"data": "voc_name"},
		        {"data": "voc_value"},
		        {"data": "voc_update_time"},
		        {"data": "voc_id"}   
		   		 ];					

				var hidden_col = [2, 4];
				if (level == 6) {
					hidden_col = [4];
				} 
				var opt={"oLanguage":{"sProcessing":"處理中...",
	                                 "sLengthMenu":"顯示 _MENU_ 筆資料",
	                                 "sZeroRecords":"沒有匹配結果",
	                                 "sInfo":"顯示第 _START_ 至 _END_ 項結果，共 _TOTAL_ 項",
	                                 "sInfoEmpty":"顯示第 0 至 0 筆資料，共 0 項",
	                                 "sInfoFiltered":"(從 _MAX_ 項資料過濾)",
	                                 "sSearch":"搜索:",
	                                 "oPaginate":{"sFirst":"首頁",
	                                                      "sPrevious":"上頁",
	                                                      "sNext":"下頁",
	                                                      "sLast":"尾頁"}
	                                 },
	                                 "processing": true,
	                                 "bSort": false,
	                                 "searching": false,
	                                 "lengthChange": false,
	                                 //"lengthMenu": [[1, 25, 50, -1], [1, 25, 50, "All"]],
	                                 "data" : data,
	                                 "columns":cols,
							        "columnDefs": [ {
							            "targets": 5,
							            "data": null,
							            "defaultContent": "<div class='row'><div class='col-md-6'><button class='btn btn-warning' style='display:none'><span class='glyphicon glyphicon-pencil' aria-hidden='true'></span> 修改</button></div><div class='col-md-6'><button class='btn btn-danger'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span> 刪除</button></div></div>"
							        }, {
							            "targets": hidden_col,
							            "visible": false
							        } ],
								      "fnInfoCallback": function( oSettings, iStart, iEnd, iMax, iTotal, sPre ) {
									    $('#table_list tbody tr td:nth-child(2)').attr('contenteditable', true).on('blur', function() {
									    	var dd = table.row( $(this).parents('tr') ).data();
									    	//console.log(dd['voc_name']);
									    	if (dd['voc_name'] != $(this).parents('tr').find('td:eq(1)').text()) {
									    		//$(this).parents('tr').find('button.btn-warning').attr('disabled', false);
									    		$(this).parents('tr').find('button.btn-warning').show();
									    	} else {
									    		//$(this).parents('tr').find('button.btn-warning').attr('disabled', true);
									    		$(this).parents('tr').find('button.btn-warning').hide();
									    	}
									    })
						 				if (level == 6) {
									    	$('#table_list tbody tr td:nth-child(3)').attr('contenteditable', true).on('keypress', function(e) {
									    		var dd = table.row( $(this).parents('tr') ).data();
												event = e || window.event;
												var charCode = (typeof event.which == "number") ? event.which : event.keyCode;
												console.log(charCode);
									    		//if(charCode < 48 || charCode > 57){
									    		if ((charCode >= 48 && charCode <= 57) || (charCode == 8)) { 	
									    			return true;
									    		} else {
									    			return false;
									    		}
									    		
									    	}).on('blur', function(e) {
									    		var dd = table.row( $(this).parents('tr') ).data();
												event = e || window.event;
										    	if (dd['voc_name'] != $(this).parents('tr').find('td:eq(1)').text() || dd['voc_value'] != $(this).parents('tr').find('td:eq(2)').text()) {
										    		//$(this).parents('tr').find('button.btn-warning').attr('disabled', false);
										    		$(this).parents('tr').find('button.btn-warning').show();
										    	} else {
										    		//$(this).parents('tr').find('button.btn-warning').attr('disabled', true);
										    		$(this).parents('tr').find('button.btn-warning').hide();
										    	}							
									    	}).on('input', function(e) {
									    		var temp_char = $(this).parents('tr').find('td:eq(2)').text();
												temp_char = temp_char.replace(/[^0-9]/g, '');
												//var start = this.selectionStart,
												//end = this.selectionEnd;					
												$(this).parents('tr').find('td:eq(2)').text(temp_char);
												$(this).parents('tr').find('td:eq(2)').focusEnd();
												//this.setSelectionRange(start, end);
									    	}).on('focus', function() {
												 //this.value = this.value;
											});		    	
									    } 									    
									  }						        	                                
	           };
	           	
	           	$("#table_list").DataTable().destroy();
	           
				var table = $('#table_list').DataTable(opt);	
			    $('#table_list tbody tr td:nth-child(2)').attr('contenteditable', true).on('blur', function() {
			    	var dd = table.row( $(this).parents('tr') ).data();
			    	//console.log(dd['voc_name']);
			    	if (dd['voc_name'] != $(this).parents('tr').find('td:eq(1)').text()) {
			    		//$(this).parents('tr').find('button.btn-warning').attr('disabled', false);
			    		$(this).parents('tr').find('button.btn-warning').show();
			    	} else {
			    		//$(this).parents('tr').find('button.btn-warning').attr('disabled', true);
			    		$(this).parents('tr').find('button.btn-warning').hide();
			    	}
			    })

			    var send_val = '';
			    if (level == 6) {
			    	$('#table_list tbody tr td:nth-child(3)').attr('contenteditable', true).on('keypress', function(e) {
			    		var dd = table.row( $(this).parents('tr') ).data();
						event = e || window.event;
						var charCode = (typeof event.which == "number") ? event.which : event.keyCode;
						console.log(charCode);
			    		//if(charCode < 48 || charCode > 57){
			    		if ((charCode >= 48 && charCode <= 57) || (charCode == 8)) { 	
			    			return true;
			    		} else {
			    			return false;
			    		}
			    		
			    	}).on('blur', function(e) {
			    		var dd = table.row( $(this).parents('tr') ).data();
						event = e || window.event;
				    	if (dd['voc_name'] != $(this).parents('tr').find('td:eq(1)').text() || dd['voc_value'] != $(this).parents('tr').find('td:eq(2)').text()) {
				    		//$(this).parents('tr').find('button.btn-warning').attr('disabled', false);
				    		$(this).parents('tr').find('button.btn-warning').show();
				    	} else {
				    		//$(this).parents('tr').find('button.btn-warning').attr('disabled', true);
				    		$(this).parents('tr').find('button.btn-warning').hide();
				    	}							
			    	}).on('input', function(e) {
			    		var temp_char = $(this).parents('tr').find('td:eq(2)').text();
						temp_char = temp_char.replace(/[^0-9]/g, '');
						//var start = this.selectionStart,
						//end = this.selectionEnd;					
						$(this).parents('tr').find('td:eq(2)').text(temp_char);
						$(this).parents('tr').find('td:eq(2)').focusEnd();
						//this.setSelectionRange(start, end);
			    	}).on('focus', function() {
						 //this.value = this.value;
					});		    	
			    } 
				$('#table_list tbody').unbind('click').on( 'click', 'button.btn-danger', function () {
			        var dd = table.row( $(this).parents('tr') ).data();
			        if(confirm('確定刪除第' + dd['no'] + '筆[' + dd['voc_name'] + ']及其以下子項嗎?')) {
				        $.ajax({
				            url: "api.php",
				            type: "post",
				            data:'act=deleteVoc&level=' + level + '&voc_id=' + dd['voc_id'],
				            success: function(response) {
				            	if (response == "OK") {
				            		//table.destroy();
				            		loadLevelData(level, pid);
				            		console.log(level, pid);
				            		//table.row('.selected').remove().draw( false );
				            	} else {
				            		alert(response);
				            	}
				               
				            }            
				        });			        	
			        }
			    } ).on( 'click', 'button.btn-warning', function () {
			    	var btn_edit = this;
			        var dd = table.row( $(this).parents('tr') ).data();
			        var value_msg = "";
			        if (level == 6) {
			        	value_msg = " 數值為["+$(this).parents('tr').find('td:eq(2)').text()+"]";
			        }
			        if(confirm('確定修改第' + dd['no'] + '筆名稱為[' + $(this).parents('tr').find('td:eq(1)').text() + ']' + value_msg + '嗎?')) {
			        	send_val = '&voc_value=' + $(this).parents('tr').find('td:eq(2)').text();
			        	//var pid = $("select.dropdown-level:last option:selected").val();
			        	var new_name = $(this).parents('tr').find('td:eq(1)').text();
				        $.ajax({
				            url: "api.php",
				            type: "post",
				            data:'act=updateVoc&voc_name=' + new_name + '&voc_id=' + dd['voc_id'] + '&voc_level=' + level + send_val + '&voc_pid=' + pid,
				            success: function(response) {
				            	if (response == "OK") {
				            		//table.destroy();
				            		//loadLevelData(level, pid);
				            		//table.row('.selected').remove().draw( false );
				            		$(btn_edit).hide();
				            		var option_eq = dd['no'];
				            		dd['voc_name'] = new_name;
				            		if (level < 6) {
				            			$(".dropdown-area .dropdown-level:last option:eq("+option_eq+")").text(new_name);
				            			$("#dropdown_lv" + level).selectpicker('refresh');
				            		} else if (level == 6) {
				            			dd['voc_value'] = $(this).parents('tr').find('td:eq(2)').text();
				            		}
				            	} else {
				            		alert(response);
				            	}
				               
				            }            
				        });			        	
			        }
			    } );
			    if (active == 1 && level == 5 && lv5_name != "") {
			    	if (typeof $("#dropdown_lv5 option:contains('"+lv5_name+"')").val() !== 'undefined') {
			    		$("#dropdown_lv5").val($("#dropdown_lv5 option:contains('"+lv5_name+"')").val());
			    		$("#dropdown_lv5").selectpicker('refresh');
			    		loadLevelData(6, $("#dropdown_lv5 option:contains('"+lv5_name+"')").val());
			    	}
			    }		    
			}    
		},
		error: function(jqXHR, textStatus, errorThrown) {
			console.log(errorThrown);	
		}
	});		
}

function changeLevelDropdown(level, id) {

	var next_level = level + 1;
	var dropdown_id = "dropdown_lv" + next_level;
	if (level <= 5) {
		//if (active != 2) {
			for (i = next_level; i <= 5; i++) {
				if ($("#dropdown_lv"+i).length) {
					$("#dropdown_lv"+i).selectpicker('destroy').remove();
				}
			}
			if (id != 0 && next_level < 6) {
				$(".dropdown-area").append(' <select class="selectpicker dropdown-level" data-style="btn-mute" id="' + dropdown_id + '" data-level="' + next_level + '"><option value="">--請選擇--</option></select>');
			}
		//}
		$("#"+dropdown_id).selectpicker('refresh');
		if ($("select.selectpicker:last option:selected").val() == "") {
			var last_idx = $(".dropdown-area select.selectpicker").length - 2;
			id =  $("select.selectpicker:eq(" + last_idx + ") option:selected").val();			
		}
		if (level == 1 && id == "") {
			next_level = 1;
			id = 0;
		}
		loadLevelData(next_level, id);
	}
	$("#table_list_wrapper").show();
	$("#table_add").hide();
	$("#add_btn").show();
	$("#return_btn").hide();
	$("#add_row").hide();	
	if (active == 1 && level == 5 && $("#dropdown_lv5").val() != "") {
		lv5_name = $("select.selectpicker:last option:selected").text();
	}
}

function getLv1Stat(id) {
    $.ajax({
        url: "api.php",
        type: "post",
        data: {	act : 'lv1_stat',
        		pid : id
    			},
        success: function(response) {
        	var json = JSON.parse(response);
        	var no = 0;
        	var total = 0;
        	var one_td = '';
        	$("#table_system_stat tbody").empty();
    		$.each(json, function(i, item) {
    			total += parseInt(item.cnt);
    			no = i + 1;
    			if (i == 0) {
    				one_td = "<td></td>";
    			} else {
    				one_td = "";
    			}
				$("#table_system_stat tbody").append('<tr><td>'+no+'</td><td>'+item.voc_name+'</td><td>'+item.cnt+'</td>'+one_td+'</tr>');
			})	
			$("#table_system_stat tbody tr:first td:eq(3)").text(total).attr('rowSpan', json.length).css('vertical-align', 'middle');    			
        }          
    });	
}

function loadUserData() {
    $.ajax({
        url: "api.php",
        type: "post",
        data: {act : 'getUser'},
		dataType:"json",
		success: function(data, textStatus, jqXHR) {
			$.each(data, function(index, item) {
				item.auth = USER_TYPE[item.usr_type - 1];   
			})				
			cols = [
	        {"data": "usr_account"},
	        {"data": "usr_password"},
	        {"data": "usr_name"},
	        {"data": "auth"},
	        {"data": "usr_id"} 
	   		 ];	            	
			var opt={"oLanguage":{"sProcessing":"處理中...",
                             "sLengthMenu":"顯示 _MENU_ 筆資料",
                             "sZeroRecords":"沒有匹配結果",
                             "sInfo":"顯示第 _START_ 至 _END_ 項結果，共 _TOTAL_ 項",
                             "sInfoEmpty":"顯示第 0 至 0 筆資料，共 0 項",
                             "sInfoFiltered":"(從 _MAX_ 項資料過濾)",
                             "sSearch":"搜索:",
                             "oPaginate":{"sFirst":"首頁",
                                                  "sPrevious":"上頁",
                                                  "sNext":"下頁",
                                                  "sLast":"尾頁"}
                             },
                             "processing": true,
                             "bSort": false,
                             "searching": false,
                             "lengthChange": false,
                             //"lengthMenu": [[1, 25, 50, -1], [1, 25, 50, "All"]],
                             "data" : data,
                             "columns":cols,
					        "columnDefs": [ {
					            "targets": 5,
					            "data": null,
					            "defaultContent": "<button class='btn btn-warning edit_btn' data-toggle='modal' data-target='#editModal'><span class='glyphicon glyphicon-pencil' aria-hidden='true'></span> 修改</button> <button class='btn btn-danger'><span class='glyphicon glyphicon-trash' aria-hidden='true'></span> 刪除</button>"
					        }, {
					            "targets": 4,
					            "visible": false
					        }]                    
       		};
	       	$("#table_user").DataTable().destroy();        
			var table = $('#table_user').DataTable(opt);	
			$('#table_user tbody').unbind('click').on( 'click', 'button.btn-danger', function () {
		        var dd = table.row( $(this).parents('tr') ).data();
		        if(confirm('確定刪除帳號[' + dd['usr_account'] + ']嗎?')) {
			        $.ajax({
			            url: "api.php",
			            type: "post",
			            data:'act=deleteUser&usr_id=' + dd['usr_id'],
			            success: function(response) {
			            	if (response == "OK") {
			            		//table.destroy();
			            		loadUserData();
			            		//table.row('.selected').remove().draw( false );
			            	} else {
			            		alert(response);
			            	}
			               
			            }            
			        });			        	
		        }
		    } ).on( 'click', 'button.btn-warning', function () {
		        var dd = table.row( $(this).parents('tr') ).data();
		        $("#usr_account").val(dd["usr_account"]).attr('readonly',  true);
		        $("#usr_password").val(dd["usr_password"]);
		        $("#usr_name").val(dd["usr_name"]);
		        $("#usr_type").val(dd["usr_type"]).selectpicker('refresh');
		        $("#usr_id").val(dd["usr_id"]);
				$(".modal-title").text("修改帳號");
		    });
		}
    });	
}

function loadimportData(pid) {
	$.ajax({
		url:'api.php',
		data: {
			act:'getLevelData',
			lv:3,
			pid: pid																
		},
		type:"POST",
		dataType:"json",
		success: function(data, textStatus, jqXHR) {
			$.each(data, function(index, item) {
				item.no = index+1;			    
			})

			cols = [
	        {"data": "no"},
	        {"data": "voc_name"},
	        {"data": "voc_update_time"},
	        {"data": "voc_id"}   
	   		 ];					
	   		var buttons =  "<button class='btn btn-success'><span class='glyphicon glyphicon-export' aria-hidden='true'></span> 匯出</button> <button class='btn btn-info' data-toggle='modal' data-target='#uploadModal'><span class='glyphicon glyphicon-import' aria-hidden='true'></span> 匯入</button>";
			if (active == 2) {
				buttons = "<button class='btn btn-primary'> 預覽</button> <button class='btn btn-success'><span class='glyphicon glyphicon-save' aria-hidden='true'></span> 選擇</button>";
			}
			var hidden_col = [3];
			var opt={"oLanguage":{"sProcessing":"處理中...",
                                 "sLengthMenu":"顯示 _MENU_ 筆資料",
                                 "sZeroRecords":"沒有匹配結果",
                                 "sInfo":"顯示第 _START_ 至 _END_ 項結果，共 _TOTAL_ 項",
                                 "sInfoEmpty":"顯示第 0 至 0 筆資料，共 0 項",
                                 "sInfoFiltered":"(從 _MAX_ 項資料過濾)",
                                 "sSearch":"搜索:",
                                 "oPaginate":{"sFirst":"首頁",
                                                      "sPrevious":"上頁",
                                                      "sNext":"下頁",
                                                      "sLast":"尾頁"}
                                 },
                                 "processing": true,
                                 "bSort": false,
                                 "searching": false,
                                 "lengthChange": false,
                                 //"lengthMenu": [[1, 25, 50, -1], [1, 25, 50, "All"]],
                                 "data" : data,
                                 "columns":cols,
						        "columnDefs": [ {
						            "targets": 4,
						            "data": null,
						            "defaultContent": buttons
						        }, {
						            "targets": hidden_col,
						            "visible": false
						        } ]	                                
           };
           	
           	$("#table_import").DataTable().destroy();
           
			var table = $('#table_import').DataTable(opt);
			if (active == 2) {
				$('#table_import tbody').unbind('click').on( 'click', 'button.btn-success', function () {
			        var dd = table.row( $(this).parents('tr') ).data();
		        	$.ajax({
						url:'api.php',
						data: {
							act:'getLevelData',
							lv:4,
							pid: dd['voc_id']																
						},
						type:"POST",
						dataType:"json",
						success: function(data, textStatus, jqXHR) {
							$.each(data, function(index, item) {

								if ($("#tab-content").find(".tab-pane.active select[name='lv4[]'] option[value='"+item.voc_id+"']").length == 0) {
									$("#tab-content").find(".tab-pane.active").find("select[name='lv4[]']").append("<option value='"+item.voc_id+"'>"+item.voc_name+"</option>");
								}
							});
						},
						error: function(jqXHR, textStatus, errorThrown) {
							console.log(errorThrown);	
						}
					});	
			    } ).on( 'click', 'button.btn-primary', function () {
			        var dd = table.row( $(this).parents('tr') ).data();
			        console.log(dd['voc_name']);
		        	$.ajax({
						url:'api.php',
						data: {
							act:'getLevelData',
							lv:4,
							pid: dd['voc_id']																
						},
						type:"POST",
						dataType:"json",
						success: function(data, textStatus, jqXHR) {
							$("#title_name").text(dd['voc_name']);
							$("#preview_lv4 tbody").empty();
							if (data.length == 0) {
								$("#preview_lv4 tbody").append("<tr><td align='center'>尚無資料</td></tr>");
							} else {
								$.each(data, function(index, item) {
									$("#preview_lv4 tbody").append("<tr><td>"+item.voc_name+"</td></tr>");
								});
							}
						},
						error: function(jqXHR, textStatus, errorThrown) {
							console.log(errorThrown);	
						}
					});			    	
			    	$("#previewModal").modal("show");
			    });	
			} else {
				$('#table_import tbody').unbind('click').on( 'click', 'button.btn-success', function () {
			        var dd = table.row( $(this).parents('tr') ).data();
			        location.href="export_voc.php?pid=" + dd['voc_id'];		        	
			    } ).on( 'click', 'button.btn-info', function () {
			        var dd = table.row( $(this).parents('tr') ).data();
			        $("#uploadModal #voc_id1").val($("#dropdown_lv1").val());
			        $("#uploadModal #voc_id2").val($("#dropdown_lv2").val());
			        $("#uploadModal #voc_id3").val(dd['voc_id']);
			         $("#uploadModal #upload_file").val("");
			    });					
			}
				    
		},
		error: function(jqXHR, textStatus, errorThrown) {
			console.log(errorThrown);	
		}
	});	
}

function insertProject(type) {
	var project_data = {};
	var project_data_array = [];
	if (pro_id == "") {
		project_data["pro_id"] = "";
	} else {
		if (type == 'save') {
			project_data["pro_id"] = pro_id;
		} else {
			project_data["pro_id"] = "";
		}			
	}
	if (pro_name == "") {
		project_data["pro_name"] = $("#pro_name").val();
	} else {
		if (type == 'save') {
			project_data["pro_name"] = pro_name;
		} else {
			project_data["pro_name"] = $("#pro_name").val();
		}
		
	}
	project_data["pro_type"] = 0;

	//console.log($("#pro_name").val() + '==' + $("#project_display_name").text());
	if (($("#pro_name").val() == $("#project_display_name").text()) && type != "save") {
		alert('專案名稱已存在!');
	} else {
		$.each($("#tab-list li"), function(index, item) {
			var tab_data = {};
			var tab_data_arr = [];
			var tab_data_select = [];
			$.each($("select[name='lv4[]']:eq("+ index +") option"), function(k, v) {
				tab_data_arr.push($(this).val());
			})
			$.each($("select[name='lv4s[]']:eq("+ index +") option"), function(k, v) {
				tab_data_select.push($(this).val());
			})			
			tab_data["tab_name"] = $(this).find("span:first").text();
			tab_data["data"] = tab_data_arr;
			tab_data["data_select"] = tab_data_select;
			project_data_array.push(tab_data);	
		});
		project_data['data'] = project_data_array;
	    $.ajax({
	        url: "api.php",
	        type: "post",
	        data: {
	        	act : "insertProject",
	        	project_data : JSON.stringify(project_data)
	        },
	        success: function(response) {
	        	data = JSON.parse(response);
	        	if (data.pro_id != '') {
	        		$("#form_project input").val('');
					$('#saveModal').modal('hide'); 
					pro_id = data.pro_id;
					pro_name = data.pro_name;
					$("#project_display_name").text(pro_name);
					alert("專案[" + pro_name + "]儲存成功");
					location.reload();
					$("#btn_delete").parent("li").show();         		
	        	} else {
	        		alert(response);
	        	}
	           
	        }            
	    });			
	}
	
}

function keypress(_this){
    _this.value = _this.value.replace(/[^0-9]/g, '');
}

$.fn.focusEnd = function() {
    $(this).focus();
    var tmp = $('<span />').appendTo($(this)),
        node = tmp.get(0),
        range = null,
        sel = null;

    if (document.selection) {
        range = document.body.createTextRange();
        range.moveToElementText(node);
        range.select();
    } else if (window.getSelection) {
        range = document.createRange();
        range.selectNode(node);
        sel = window.getSelection();
        sel.removeAllRanges();
        sel.addRange(range);
    }
    tmp.remove();
    return this;
}

function htmlDecode(value) {
  return $("<textarea/>").html(value).text();
}

function htmlEncode(value) {
  return $('<textarea/>').text(value).html();
}