﻿<?php
require_once 'common.php';

if ($_SESSION['login_status'] == "") header( 'Location: login.php' );
if ($_SESSION['usr_type'] <> 1) header( 'Location: index.php' );
?>

<!--[if IE ]><![endif]-->
<!doctype html>
<!--[if IE 8 ]> <html class="no-js lt-ie9 ie8" lang="zh-TW"> <![endif]-->
<!--[if IE 9 ]> <html class="no-js lt-ie10 ie9" lang="zh-TW"> <![endif]-->
<!--[if (gte IE 10)|!(IE)]><!-->
<html lang="zh-TW">
<!--<![endif]-->

<head>
	<title><?php echo TITLE?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/bootstrap-theme.css">
	<link rel="stylesheet" href="css/bootstrap-select.css">
	<link rel="stylesheet" href="css/main.css?1540876269">
	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="js/bootstrap.js"></script>
	<script src="js/bootstrap-select.js"></script>
    <link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css">
  	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
	<script type="text/javascript" charset="utf8" src="js/jquery.dataTables.js"></script>		
	<script src="js/main.js?1513089943"></script>
	<script type="text/javascript" src="js/jquery.validate.js"></script>
	<script type="text/javascript" src="js/messages_zh_TW.js"></script>
	<script type="text/javascript" src="js/additional-methods.js"></script>		
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" />
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css" />	
</head>

<body>
	<div class="wrap">
		<div class="header_wrap">
 <?php include ('nav.php'); ?>	
		</div>
		<div class="content_wrap">
			<div class="row">
		        <div class="col-md-10 col-md-offset-1">
					<button type="button" class="btn btn-primary pull-right" id="add_btn" data-toggle="modal" data-target="#editModal"><span class=" glyphicon glyphicon-plus" aria-hidden="true"></span> 新增</button>
		        </div>
		    </div>    
			<div class="row">
		        <div class="col-md-10 col-md-offset-1">
					<table id="table_user" class="display">
				    <thead>
				        <tr>
				            <th class="text-center">帳號</th>
				            <th class="text-center">密碼</th>
				            <th class="text-center">使用者姓名</th>
				            <th class="text-center">權限</th>
				            <th class="text-center">維護</th>
				            <th class="text-center">維護</th>					            					            
				        </tr>
				    </thead>
				    <tbody>

				    </tbody>
				</table>
		        </div>
			</div>
					
		</div>
		<footer class="footer">

		</footer>
	</div>

<!-- Modal -->
<div id="editModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">新增帳號</h4>
      </div>
      <div class="modal-body">
		<form id="form_user">
		  <div class="form-group">
		    <label for="usr_account">帳號:</label>
		    <input type="text" class="form-control" name="usr_account" id="usr_account">
		  </div>
		  <div class="form-group">
		    <label for="usr_password">密碼:</label>
		    <input type="text" class="form-control" name="usr_password" id="usr_password">
		  </div>
		  <div class="form-group">
		    <label for="usr_name">姓名:</label>
		    <input type="text" class="form-control" name="usr_name" id="usr_name">
		  </div>		  
		  <div class="form-group">
		    <label for="usr_name">權限:</label>
			<select class="selectpicker" data-style="btn-default" name="usr_type" id="usr_type">
			  <option value="">--請選擇--</option>
			  <?php
			  	foreach (USER_TYPE as $key => $value) {
			  ?>
			  <option value="<?php echo $key+1?>"><?php echo $value?></option>
			  <?php
			  	}
			  ?>
			</select>
		  </div>
		  <input type="hidden" name="usr_id" id="usr_id">
		  <button type="submit" class="btn btn-primary">儲存</button>
		</form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>

  </div>
</div>	
</body>
</html>