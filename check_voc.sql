SELECT          lv1.voc_id AS id1, lv2.voc_id AS id2, lv3.voc_id AS id3, lv4.voc_id AS id4, lv5.voc_id AS id5, lv5.voc_name as name5, lv6.voc_id AS id6, lv6.voc_name as name6
FROM              (SELECT          voc_id, voc_pid, voc_name
                    FROM               vocabulary
                    WHERE           voc_level = 6) AS lv6 LEFT  JOIN
                  (SELECT          voc_id, voc_pid, voc_level, voc_name
                    FROM               vocabulary 
                    WHERE           voc_level = 5) AS lv5 ON lv6.voc_pid = lv5.voc_id LEFT  JOIN
                  (SELECT          voc_id, voc_pid, voc_level
                    FROM               vocabulary 
                    WHERE           voc_level = 4) AS lv4 ON lv5.voc_pid = lv4.voc_id LEFT  JOIN
                  (SELECT          voc_id, voc_pid, voc_level
                    FROM               vocabulary 
                    WHERE           voc_level = 3) AS lv3 ON lv4.voc_pid = lv3.voc_id LEFT  JOIN
                  (SELECT          voc_id, voc_pid, voc_level
                    FROM               vocabulary 
                    WHERE           voc_level = 2) AS lv2 ON lv3.voc_pid = lv2.voc_id LEFT  JOIN
                  (SELECT          voc_id, voc_level
                    FROM               vocabulary 
                    WHERE           voc_level = 1) AS lv1 ON lv2.voc_pid = lv1.voc_id