<?php
require_once 'libs/db.class.php';
require_once 'config.php';

session_start();
$db = new MyDB($DSN);
$act = $_POST["act"];

if ($act == "getLevelData") {
	$lv = $_POST["lv"];
	$pid = $_POST["pid"];
	$sql = "select voc_id, voc_name, voc_value, format(voc_update_time, 'yyyy/MM/dd HH:mm:ss') as voc_update_time from vocabulary where voc_pid = {$pid}";
	$rs = $db->obj->getAll($sql);
	echo json_encode($rs);
} elseif ($act == "insertVoc") {
	$voc_name = $_POST["voc_name"];
	if (isset($_POST["voc_value"])) {
		$voc_value = $_POST["voc_value"];
	}
	$level = $_POST["level"];
	$pid = $_POST["pid"];
	if (is_array($voc_name)) {
		foreach ($voc_name as $key => $value) {
			if (!empty($value)) {
				$voc_val = ($level == 6) ? $voc_value[$key] : '';
		        $options = array(
	            'type' => 'insert',
	            'tables' => 'vocabulary',
	            'values' => array(
					'voc_name' => trim($value),
					'voc_value' => (int)$voc_val,
					'voc_level' => (int)$level,
					'voc_pid' => (int)$pid,
					'voc_create_user' => trim($_SESSION["usr_account"]),
					'voc_update_user' => trim($_SESSION["usr_account"])
					)
		        );
		        //$sql = $db->createQueryString($options);
		        if ($level == 6) {
		       		$sql = "insert into vocabulary (voc_name, voc_value, voc_level, voc_pid, voc_create_user, voc_update_user) values (N'" . trim($value) . "', {$voc_val}, {$level}, {$pid}, '" . $_SESSION["usr_account"] . "', '" . $_SESSION["usr_account"] . "')";
		       	} else {
		       		$sql = "insert into vocabulary (voc_name, voc_level, voc_pid, voc_create_user, voc_update_user) values (N'" . trim($value) . "', {$level}, {$pid}, '" . $_SESSION["usr_account"] . "', '" . $_SESSION["usr_account"] . "')";
		       	}
		        $db->query($sql);
		        if ($level == 4) {
		        	$sql = "SELECT IDENT_CURRENT ('vocabulary') AS Current_Identity";
					$lv4_id = $db->obj->getOne($sql);
					$sql = "SELECT DISTINCT voc_name FROM vocabulary WHERE voc_level = 5";
					$rs = $db->obj->getAll($sql);
					$voc_5 = array();
					foreach ($rs as $key => $value) {
						$voc_5[] = "(N'" . $value["voc_name"] . "', 5, " . $lv4_id . ", '" . $_SESSION["usr_account"] . "', '" . $_SESSION["usr_account"] . "')";
					}
					$values = implode(",", $voc_5);
					$sql = "insert into vocabulary (voc_name, voc_level, voc_pid, voc_create_user, voc_update_user) values " . $values;
					//print $sql;
					$db->query($sql);
		        }
		    }
	    }

	}
	echo "OK";
} elseif ($act == "deleteVoc") {
	$level = $_POST["level"];
	$voc_id = $_POST["voc_id"];
	$where = " where lv" . $level . ".voc_id=" . $voc_id;
	$sql = "select lv1.voc_id as id1, lv2.voc_id as id2, lv3.voc_id as id3, lv4.voc_id as id4, lv5.voc_id as id5, lv6.voc_id as id6  from (
			(select voc_id from vocabulary where voc_level = 1) lv1 
			left join (select voc_id, voc_pid, voc_level from vocabulary where voc_level = 2 ) lv2 on lv1.voc_id = lv2.voc_pid  
			left join (select voc_id, voc_pid, voc_level from vocabulary where voc_level = 3 ) lv3 on lv2.voc_id = lv3.voc_pid  
			left join (select voc_id, voc_pid, voc_level from vocabulary where voc_level = 4 ) lv4 on lv3.voc_id = lv4.voc_pid 
			left join (select voc_id, voc_pid, voc_level from vocabulary where voc_level = 5 ) lv5 on lv4.voc_id = lv5.voc_pid 
			left join (select voc_id, voc_pid, voc_level from vocabulary where voc_level = 6 ) lv6 on lv5.voc_id = lv6.voc_pid 
			)" . $where;
	$rs = $db->obj->getAll($sql);
	$voc_ids = array();
	foreach ($rs as $key => $value) {
		for ($i = $level; $i <= 6; $i++) {
			$col_name = "id" . $i;
			if (!in_array($value[$col_name], $voc_ids) && !is_null($value[$col_name])) {
				$voc_ids[] = $value[$col_name];
			}
		}
    }
    $voc_ids = implode(', ', $voc_ids);
	$sql = "delete from vocabulary where voc_id in ({$voc_ids})";
	if ($db->query($sql)) {
		echo "OK";
	}
} elseif ($act == "chkVocExist") {
	$voc_name = $_POST["voc_names"];
	$voc_list = explode(",", $voc_name);
	$chk_list = array();
	if (is_array($voc_list)) {
		foreach ($voc_list as $key => $value) {
			if (!empty($value)) {
				$sql = "select voc_id from vocabulary where voc_name = N'{$value}'";
		        $rs = $db->obj->getOne($sql);
		        if ($rs) {
		        	$chk_list[] = "Y";
		        } else {
		        	$chk_list[] = "N";
		        }
		    } else {
		    	$chk_list[] = "N";
		    }
	    }
	}
	//var_export($chk_list);
	if (!in_array("Y", $chk_list)) {
		echo "true";
	} else {
		//echo "false";
		echo json_encode($chk_list);
	}
} elseif ($act == "chkVocExist2") {
	$voc_name = $_POST["voc_name"];
	$level = $_POST["level"];
	$pid = $_POST["pid"];
	$chk_list = array();
	if (is_array($voc_name)) {
		foreach ($voc_name as $key => $value) {
			if (!empty($value)) {
				if ($level < 3) {
					$sql = "select voc_id from vocabulary where voc_name = N'{$value}' and voc_level < 3";
				} else {
					$sql = "select voc_id from vocabulary where voc_name = N'{$value}' and voc_pid = {$pid}";
				}

		        $rs = $db->obj->getOne($sql);
		        if ($rs) {
		        	$chk_list[] = "Y";
		        } else {
		        	$chk_list[] = "N";
		        }
		    } else {
		    	$chk_list[] = "N";
		    }
	    }
	}
	if (!in_array("Y", $chk_list)) {
		echo "true";
	} else {
		//echo "false";
		echo json_encode($chk_list);
	}
} elseif ($act == "updateVoc") {
	$voc_name = $_POST["voc_name"];
	$voc_id = $_POST["voc_id"];
	$voc_pid = $_POST["voc_pid"];
	$voc_level = $_POST["voc_level"];
	$voc_value = $_POST["voc_value"];
	if ($voc_level == 1) {
		$sql = "select voc_id from vocabulary where voc_name = N'{$voc_name}' and voc_id <> {$voc_id}";
	} else {
		$sql = "select voc_id from vocabulary where voc_name = N'{$voc_name}' and voc_pid = {$voc_pid} and voc_id <> {$voc_id}";
	}

    $rs = $db->obj->getOne($sql);
    if ($rs) {
    	echo "修改名稱已存在!";
    } else {
    	if ($voc_level == 6) {
    		$sql = "update vocabulary set voc_name = N'{$voc_name}', voc_value = '{$voc_value}' where voc_id = {$voc_id}";
    	} else {
    		$sql = "update vocabulary set voc_name = N'{$voc_name}' where voc_id = {$voc_id}";
    	}

		if ($db->query($sql)) {
			echo "OK";
		}
    }
} elseif ($act == "statistics") {
	$sql = "select g.*, v.voc_name from 
			(select lv1.voc_id as id1, count(*) as cnt  from (
			(select voc_id from vocabulary where voc_level = 1) lv1 
			inner join (select voc_id, voc_pid, voc_level from vocabulary where voc_level = 2 ) lv2 on lv1.voc_id = lv2.voc_pid  
			inner join (select voc_id, voc_pid, voc_level from vocabulary where voc_level = 3 ) lv3 on lv2.voc_id = lv3.voc_pid  
			inner join (select voc_id, voc_pid, voc_level from vocabulary where voc_level = 4 ) lv4 on lv3.voc_id = lv4.voc_pid 
			inner join (select voc_id, voc_pid, voc_level from vocabulary where voc_level = 5 ) lv5 on lv4.voc_id = lv5.voc_pid 
			inner join (select voc_id, voc_pid, voc_level from vocabulary where voc_level = 6 ) lv6 on lv5.voc_id = lv6.voc_pid 
			) group by lv1.voc_id) g left join vocabulary v on g.id1 = v.voc_id order by cnt desc";
	$rs = $db->obj->getAll($sql);
	echo json_encode($rs);
} elseif ($act == "lv1_stat") {
	$pid =  $_POST["pid"];
	$sql = "select g.*, v.voc_name from 
(select lv2.voc_id as id2, count(*) as cnt  from (
			(select voc_id from vocabulary where voc_level = 1) lv1 
			inner join (select voc_id, voc_pid, voc_level from vocabulary where voc_level = 2 ) lv2 on lv1.voc_id = lv2.voc_pid  
			inner join (select voc_id, voc_pid, voc_level from vocabulary where voc_level = 3 ) lv3 on lv2.voc_id = lv3.voc_pid  
			inner join (select voc_id, voc_pid, voc_level from vocabulary where voc_level = 4 ) lv4 on lv3.voc_id = lv4.voc_pid 
			inner join (select voc_id, voc_pid, voc_level from vocabulary where voc_level = 5 ) lv5 on lv4.voc_id = lv5.voc_pid 
			inner join (select voc_id, voc_pid, voc_level from vocabulary where voc_level = 6 ) lv6 on lv5.voc_id = lv6.voc_pid 
			) where lv1.voc_id = {$pid} group by lv2.voc_id) g left join vocabulary v on g.id2 = v.voc_id order by cnt desc";
	$rs = $db->obj->getAll($sql);
	echo json_encode($rs);
} elseif ($act == "updateStatus") {
	$sql = "select top 50 lv1.voc_name as lv1, lv2.voc_name as lv2, lv6.voc_update_user, format(lv6.voc_update_time, 'yyyy/MM/dd HH:mm:ss') as voc_update_time  from (
			(select voc_id, voc_name from vocabulary where voc_level = 1) lv1 
			inner join (select voc_id, voc_pid, voc_level, voc_name from vocabulary where voc_level = 2 ) lv2 on lv1.voc_id = lv2.voc_pid  
			inner join (select voc_id, voc_pid, voc_level from vocabulary where voc_level = 3 ) lv3 on lv2.voc_id = lv3.voc_pid  
			inner join (select voc_id, voc_pid, voc_level from vocabulary where voc_level = 4 ) lv4 on lv3.voc_id = lv4.voc_pid 
			inner join (select voc_id, voc_pid, voc_level from vocabulary where voc_level = 5 ) lv5 on lv4.voc_id = lv5.voc_pid 
			inner join (select voc_id, voc_pid, voc_level, voc_update_user, voc_update_time from vocabulary where voc_level = 6 ) lv6 on lv5.voc_id = lv6.voc_pid 
			) order by voc_update_time desc";
	$rs = $db->obj->getAll($sql);
	echo json_encode($rs);
} elseif ($act == "getUser") {
	$sql = "select usr_id, usr_account, usr_password, usr_name, usr_type from [user] where usr_account <> 'admin' order by usr_id";
	$rs = $db->obj->getAll($sql);
	echo json_encode($rs);
} elseif ($act == "chkUserAccountExist") {
	$usr_account = filter_var($_POST['usr_account'], FILTER_SANITIZE_STRING);
	$sql = "select usr_account from [user] where usr_account = '{$usr_account}'";
	$rs = $db->obj->getRow($sql);
	if ($rs) {
		echo "false";
	} else {
		echo "true";
	}
} elseif ($act == "insertUser") {
	$usr_account = filter_var($_POST['usr_account'], FILTER_SANITIZE_STRING);
	$usr_password = filter_var($_POST['usr_password'], FILTER_SANITIZE_STRING);
	$usr_name = filter_var($_POST['usr_name'], FILTER_SANITIZE_STRING);
	$usr_type = filter_var($_POST['usr_type'], FILTER_VALIDATE_INT);
	$sql = "insert into [user] (usr_account, usr_password, usr_name, usr_type, usr_create_user, usr_update_user) values ('" . $usr_account . "', '" . $usr_password . "', N'" . $usr_name . "', " . $usr_type . ", '" . $_SESSION["usr_account"] . "', '" . $_SESSION["usr_account"] . "')";

	if ($db->query($sql)) {
		echo "OK";
	}
} elseif ($act == "deleteUser") {
	$usr_id = filter_var($_POST['usr_id'], FILTER_VALIDATE_INT);
	$sql = "delete from [user] where usr_id = {$usr_id}";
	if ($db->query($sql)) {
		echo "OK";
	}
} elseif ($act == "updateUser") {
	$usr_id = filter_var($_POST['usr_id'], FILTER_VALIDATE_INT);
	$usr_account = filter_var($_POST['usr_account'], FILTER_SANITIZE_STRING);
	$usr_password = filter_var($_POST['usr_password'], FILTER_SANITIZE_STRING);
	$usr_name = filter_var($_POST['usr_name'], FILTER_SANITIZE_STRING);
	$usr_type = filter_var($_POST['usr_type'], FILTER_VALIDATE_INT);
	$sql = "update [user] set usr_account = '" . $usr_account . "', usr_password = '" . $usr_password . "', usr_name = N'" . $usr_name . "', usr_type = " . $usr_type . ", usr_create_user = '" . $_SESSION["usr_account"] . "', usr_update_user = '" . $_SESSION["usr_account"] . "' where usr_id = " . $usr_id;
	if ($db->query($sql)) {
		echo "OK";
	}
} elseif ($act == "exportVoc") {
	$level = 3;
	$voc_id = $_POST["pid"];
	$where = " where lv" . $level . ".voc_id=" . $voc_id;
	$sql = "select lv4.voc_name as voc_name4, lv5.voc_name as voc_name5, lv6.voc_name as voc_name6, lv6.voc_value  from (
			(select voc_id from vocabulary where voc_level = 1) lv1 
			inner join (select voc_id, voc_pid, voc_level from vocabulary where voc_level = 2 ) lv2 on lv1.voc_id = lv2.voc_pid  
			inner join (select voc_id, voc_pid, voc_level from vocabulary where voc_level = 3 ) lv3 on lv2.voc_id = lv3.voc_pid  
			inner join (select voc_id, voc_pid, voc_level, voc_name from vocabulary where voc_level = 4 ) lv4 on lv3.voc_id = lv4.voc_pid 
			inner join (select voc_id, voc_pid, voc_level, voc_name from vocabulary where voc_level = 5 ) lv5 on lv4.voc_id = lv5.voc_pid 
			inner join (select voc_id, voc_pid, voc_level, voc_name, voc_value from vocabulary where voc_level = 6 ) lv6 on lv5.voc_id = lv6.voc_pid 
			)" . $where;
	$rs = $db->obj->getAll($sql);
	echo json_encode($rs);
} elseif ($act == "importVoc") {
	$import_data = json_decode($_POST['import_data'], TRUE);
	$add_lv4 = 0;
	$add_lv5 = 0;
	$add_lv6 = 0;
	$upd_lv6 = 0;
	$msg = "";
	foreach ($import_data as $key => $value) {
		if ($value[0]['id4'] == "") {
			$value[0]['name4'] = filter_var($value[0]['name4'], FILTER_SANITIZE_STRING);
			$sql = "select voc_id from vocabulary where voc_name = N'" .  trim($value[0]['name4']) . "' and voc_pid = " . $value[0]['id3'];
		    $rs = $db->obj->getOne($sql);
		    if ($rs) {
		    	$lv4_id = $rs;
		    } else {
				$sql = "insert into vocabulary (voc_name, voc_level, voc_pid, voc_create_user, voc_update_user) values (N'" . trim($value[0]['name4']) . "', 4, "  . $value[0]['id3'] . ", '" . $_SESSION["usr_account"] . "', '" . $_SESSION["usr_account"] . "')";
				$db->query($sql);
				$sql = "SELECT IDENT_CURRENT ('vocabulary') AS Current_Identity";
				$lv4_id = $db->obj->getOne($sql);
				$add_lv4++;
			}
		} else {
			$lv4_id = $value[0]['id4'];
		}
		if ($value[0]['id5'] == "") {
			$value[0]['name5'] = filter_var($value[0]['name5'], FILTER_SANITIZE_STRING);
			$sql = "select voc_id from vocabulary where voc_name = N'" . trim($value[0]['name5']) . "' and voc_pid = " . $lv4_id;
		    $rs = $db->obj->getOne($sql);
		    if ($rs) {
		    	$lv5_id = $rs;
		    } else {
				$sql = "insert into vocabulary (voc_name, voc_level, voc_pid, voc_create_user, voc_update_user) values (N'" . trim($value[0]['name5']) . "', 5, "  . $lv4_id . ", '" . $_SESSION["usr_account"] . "', '" . $_SESSION["usr_account"] . "')";
				$db->query($sql);
				$sql = "SELECT IDENT_CURRENT ('vocabulary') AS Current_Identity";
				$lv5_id = $db->obj->getOne($sql);
				$add_lv5++;
			}
		} else {
			$lv5_id = $value[0]['id5'];
		}
		if ($value[0]['id6'] == "") {
			$value[0]['name6'] = filter_var($value[0]['name6'], FILTER_SANITIZE_STRING);
			$sql = "insert into vocabulary (voc_name, voc_value, voc_level, voc_pid, voc_create_user, voc_update_user) values (N'" . trim($value[0]['name6']) . "'," . $value[0]['voc_value'] . " ,6 , "  . $lv5_id . ", '" . $_SESSION["usr_account"] . "', '" . $_SESSION["usr_account"] . "')";
			$db->query($sql);
			$add_lv6++;
		} else {
			$sql = "update vocabulary set voc_name = N'" . trim($value[0]['name6']) . "', voc_value = '" . $value[0]['voc_value'] . "', voc_update_user = '" . $_SESSION["usr_account"] . "', voc_update_time = getdate()  where voc_id = " . $value[0]['id6'];
			$db->query($sql);
			$upd_lv6++;
		}
	}
	if ($add_lv4 > 0) {
		$msg .= "<p>[新增" . WORD_TYPE[4-1] . $add_lv4 . "筆]</p>";
	}
	if ($add_lv5 > 0) {
		$msg .= "<p>[新增" . WORD_TYPE[5-1] . $add_lv5 . "筆]</p>";
	}
	if ($add_lv6 > 0) {
		$msg .= "<p>[新增" . WORD_TYPE[6-1] . $add_lv6 . "筆]</p>";
	}
	if ($upd_lv6 > 0) {
		$msg .= "<p>[修改" . WORD_TYPE[6-1] . $upd_lv6 . "筆]</p>";
	}
	echo $msg;
} elseif ($act == "searchVoc") {
	$keyword = filter_var($_POST['keyword'], FILTER_SANITIZE_STRING);
	$sql = "select top 1 voc_id, voc_pid, voc_level from vocabulary where voc_level <= 3 and voc_name = N'" . trim($keyword) . "'";
	$rs = $db->obj->getRow($sql);
	$id1 = 0;
	$id2 = 0;
	$id3 = 0;
	$data = array();
	if ($rs) {
		if ($rs["voc_level"] == 1) {
			$id1 = $rs["voc_id"];
		} else if ($rs["voc_level"] == 2) {
			$id1 = $rs["voc_pid"];
			$id2 = $rs["voc_id"];
		} else if ($rs["voc_level"] == 3) {
			$id2 = $rs["voc_pid"];
			$id3 = $rs["voc_id"];
			$sql = "select voc_pid from vocabulary where voc_id = {$id2}";
			$rs = $db->obj->getRow($sql);
			$id1 = $rs["voc_pid"];
		}
		$data["id1"] = $id1;
		$data["id2"] = $id2;
		$data["id3"] = $id3;
		echo json_encode($data);
	} else {
		echo "false";
	}

} elseif ($act == "getLevelDataArr") {
	$ids_arr = $_POST["ids"];
	$ids = implode(',', $ids_arr);
	$sql = "select lv4.voc_name as voc_name4, lv6.voc_id, lv6.voc_name  from (
			(select voc_id from vocabulary where voc_level = 1) lv1 
			inner join (select voc_id, voc_pid, voc_level from vocabulary where voc_level = 2 ) lv2 on lv1.voc_id = lv2.voc_pid  
			inner join (select voc_id, voc_pid, voc_level from vocabulary where voc_level = 3 ) lv3 on lv2.voc_id = lv3.voc_pid  
			inner join (select voc_id, voc_pid, voc_level, voc_name from vocabulary where voc_level = 4 ) lv4 on lv3.voc_id = lv4.voc_pid 
			inner join (select voc_id, voc_pid, voc_level from vocabulary where voc_level = 5 ) lv5 on lv4.voc_id = lv5.voc_pid 
			inner join (select voc_id, voc_pid, voc_level, voc_name from vocabulary where voc_level = 6 ) lv6 on lv5.voc_id = lv6.voc_pid 
			) where lv4.voc_id in ($ids)";
	$rs = $db->obj->getAll($sql);
	echo json_encode($rs);
} elseif ($act == "chkProjectExist") {
	$pro_name = filter_var($_POST['pro_name'], FILTER_SANITIZE_STRING);
	$sql = "select pro_name from [project] where pro_name = N'{$pro_name}' and pro_usr_id = ". $_SESSION["usr_id"];
	$rs = $db->obj->getRow($sql);
	if ($rs) {
		echo "false";
	} else {
		echo "true";
	}
} elseif ($act == "insertProject") {
	$project_data = json_decode($_POST['project_data'], TRUE);
	$pro_id = $project_data['pro_id'];

	if (empty($pro_id)) {
		$sql = "insert into project (pro_name, pro_usr_id, pro_create_user, pro_update_user) values (N'" . trim($project_data['pro_name']) . "', "  . $_SESSION["usr_id"] . ", '" . $_SESSION["usr_account"] . "', '" . $_SESSION["usr_account"] . "')";
		$db->query($sql);
		$sql = "SELECT IDENT_CURRENT ('project') AS Current_Identity";
		$pro_id = $db->obj->getOne($sql);
	} else {
        $sql = "update project set pro_type = 0 where pro_id = {$pro_id}";
        $db->query($sql);
		$sql = "select tag_id from tag where tag_pro_id = {$pro_id}";
		$rs = $db->obj->getAll($sql);
		$ids = array();
		if ($rs) {
            foreach ($rs as $key => $value) {
                $ids[] = $value['tag_id'];
            }

            $tag_ids = implode(",", $ids);
            $sql = "delete from glossary where glo_tag_id in ({$tag_ids})";
            $db->query($sql);
            $sql = "delete from tag where tag_id in ({$tag_ids})";
            $db->query($sql);
        }

	}
	if ($project_data['data']) {

		foreach ($project_data['data'] as $tag_key => $tag_value) {

			$sql = "insert into tag (tag_pro_id, tag_name, tag_create_user, tag_update_user) values (" . $pro_id . ", N'" . $tag_value['tab_name'] . "', '" . $_SESSION["usr_account"] . "', '" . $_SESSION["usr_account"] . "')";
			$db->query($sql);
			$sql = "SELECT IDENT_CURRENT ('tag') AS Current_Identity";
			$tag_id = $db->obj->getOne($sql);
			$glo = array();

			if ($tag_value['data']) {
				foreach ($tag_value['data'] as $glo_key => $glo_value) {
					$flag = 0;
					if (in_array($glo_value, $tag_value['data_select'])) {
						$flag = 1;
					}
					$glo[] = "(" . $tag_id . ", " . $glo_value . ", " . $flag . ", '" . $_SESSION["usr_account"] . "', '" . $_SESSION["usr_account"] . "')";
				}
				$values = implode(",", $glo);
				$sql = "insert into glossary (glo_tag_id, glo_voc_id,glo_flag, glo_create_user, glo_update_user) values " . $values;
				$db->query($sql);
			}
		}
	}
	echo json_encode(array('pro_id' => $pro_id, 'pro_name' => $project_data['pro_name']));
} elseif ($act == "insertPublicProject") {
    $project_data = json_decode($_POST['project_data'], TRUE);

    $sql = "insert into project (pro_name, pro_usr_id, pro_type, pro_lv2_id, pro_create_user, pro_update_user) values (N'" . trim($project_data['pro_name']) . "', "  . $_SESSION["usr_id"] . ", 1, " . $project_data['pro_lv2_id'] . ", '" . $_SESSION["usr_account"] . "', '" . $_SESSION["usr_account"] . "')";

    $db->query($sql);
    $sql = "SELECT IDENT_CURRENT ('project') AS Current_Identity";
    $pro_id = $db->obj->getOne($sql);

//    if ($project_data['pro_lv2_id']) {
//        $sql = "select voc_id, voc_name from vocabulary where voc_pid = {$project_data['pro_lv2_id']} and voc_level = 3 order by voc_id";
//        $rs = $db->obj->getAll($sql);
//        foreach ($rs as $tag_key => $tag_value) {
//            $sql = "insert into tag (tag_pro_id, tag_name, tag_create_user, tag_update_user) values (" . $pro_id . ", N'" . $tag_value['voc_name'] . "', '" . $_SESSION["usr_account"] . "', '" . $_SESSION["usr_account"] . "')";
//            $db->query($sql);
//            $sql = "SELECT IDENT_CURRENT ('tag') AS Current_Identity";
//            $tag_id = $db->obj->getOne($sql);
//            $glo = array();
//            $sql = "select voc_id, voc_name from vocabulary where voc_pid = {$tag_value['voc_id']} and voc_level = 4 order by voc_id";
//            $glo_rs = $db->obj->getAll($sql);
//            if ($glo_rs) {
//                foreach ($glo_rs as $glo_key => $glo_value) {
//                    $flag = 1;
//
//                    $glo[] = "(" . $tag_id . ", " . $glo_value['voc_id'] . ", " . $flag . ", '" . $_SESSION["usr_account"] . "', '" . $_SESSION["usr_account"] . "')";
//                }
//                $values = implode(",", $glo);
//                $sql = "insert into glossary (glo_tag_id, glo_voc_id, glo_flag, glo_create_user, glo_update_user) values " . $values;
//                $db->query($sql);
//            }
//        }
//    }
    echo json_encode(array('pro_id' => $pro_id, 'pro_name' => $project_data['pro_name']));

} elseif ($act == "getProject") {
	$sql = "select * from project where pro_usr_id = " . $_SESSION["usr_id"] . " order by pro_id";
	$rs = $db->obj->getAll($sql);
	echo json_encode($rs);
} elseif ($act == "loadProject") {
	$pro_id = filter_var($_POST['pro_id'], FILTER_VALIDATE_INT);
	$sql = "select * from project where pro_id = {$pro_id}";
    $rs = $db->obj->getRow($sql);
    $pro_type = $rs["pro_type"];
    $pro_lv2_id = $rs["pro_lv2_id"];
    //公版
    if ($pro_type == 1) {
        $sql = "select voc_id, voc_name from vocabulary where voc_pid = {$pro_lv2_id} and voc_level = 3 order by voc_id";
        $rs = $db->obj->getAll($sql);
        $project_data = array();
        foreach ($rs as $key => $value) {
            //$project_data["tag_name"] = $value["tag_name"];
            $tag_arr = array();
            $lv4_arr = array();
            $lv6_arr = array();
            $tag_arr["tab_name"] = $value["voc_name"];
            $tag_arr["tag_id"] = $value["voc_id"];

            $sql = "select voc_id, voc_name from vocabulary where voc_pid = {$value['voc_id']} and voc_level = 4 order by voc_id";

            $rs_lv4 = $db->obj->getAll($sql);
            foreach ($rs_lv4 as $k_lv4 => $v_lv4) {
                $lv4_row = array();
                $lv4_row["voc_id"] = $v_lv4["voc_id"];
                $lv4_row["voc_name"] = $v_lv4["voc_name"];
                $lv4_arr[] = $lv4_row;
                unset($lv4_row);
            }
            $tag_arr["lv4"] = $lv4_arr;
            unset($lv4_arr);

            $lv4s_arr = array();
            foreach ($rs_lv4 as $k_lv4s => $v_lv4s) {
                $lv4s_row = array();
                $lv4s_row["voc_id"] = $v_lv4s["voc_id"];
                $lv4s_row["voc_name"] = $v_lv4s["voc_name"];
                $lv4s_row["glo_id"] = $v_lv4s["glo_id"];
                $lv4s_arr[] = $lv4s_row;
                unset($lv4s_row);
            }
            $tag_arr["lv4s"] = $lv4s_arr;
            unset($lv4s_arr);
            $project_data[] = $tag_arr;
            unset($tag_arr);
        }
    } else {
        $sql = "select tag_id, tag_name from tag where tag_pro_id = {$pro_id} order by tag_id";
        $rs = $db->obj->getAll($sql);
        $project_data = array();
        foreach ($rs as $key => $value) {
            //$project_data["tag_name"] = $value["tag_name"];
            $tag_arr = array();
            $lv4_arr = array();
            $lv6_arr = array();
            $tag_arr["tab_name"] = $value["tag_name"];
            $tag_arr["tag_id"] = $value["tag_id"];

            $sql = "select a.glo_voc_id, b.voc_name from glossary a inner join vocabulary b on a.glo_voc_id = b.voc_id where glo_tag_id = " . $value["tag_id"];
            $rs_lv4 = $db->obj->getAll($sql);
            foreach ($rs_lv4 as $k_lv4 => $v_lv4) {
                $lv4_row = array();
                $lv4_row["voc_id"] = $v_lv4["glo_voc_id"];
                $lv4_row["voc_name"] = $v_lv4["voc_name"];
                $lv4_arr[] = $lv4_row;
                unset($lv4_row);
            }
            $tag_arr["lv4"] = $lv4_arr;
            unset($lv4_arr);
            $sql = "select a.glo_id, a.glo_voc_id, b.voc_name from glossary a inner join vocabulary b on a.glo_voc_id = b.voc_id where glo_tag_id = " . $value["tag_id"] . " and glo_flag = 1";
            $rs_lv4s = $db->obj->getAll($sql);
            $lv4s_arr = array();
            foreach ($rs_lv4s as $k_lv4s => $v_lv4s) {
                $lv4s_row = array();
                $lv4s_row["voc_id"] = $v_lv4s["glo_voc_id"];
                $lv4s_row["voc_name"] = $v_lv4s["voc_name"];
                $lv4s_row["glo_id"] = $v_lv4s["glo_id"];
                $lv4s_arr[] = $lv4s_row;
                unset($lv4s_row);
            }
            $tag_arr["lv4s"] = $lv4s_arr;
            unset($lv4s_arr);
            $project_data[] = $tag_arr;
            unset($tag_arr);
        }
    }

	echo json_encode($project_data);
} elseif ($act == "chkLV4Exist") {
	$lv4 = $_POST['lv4'];
	$return = array();
	foreach ($lv4 as $key => $value) {
		$sql = "select voc_id from vocabulary where voc_id = {$value}";
		$rs = $db->obj->getOne($sql);
		if ($rs) {
			$return[] = $rs;
		} else {
			$return[] = "N";
		}
	}
	echo json_encode($return);
} elseif ($act == "deleteProject") {
	$pro_id = filter_var($_POST['pro_id'], FILTER_VALIDATE_INT);
	$sql = "select tag_id from tag where tag_pro_id = " . $pro_id;
	$rs = $db->obj->getAll($sql);
	$tag_ids = array();
	if ($rs) {
		foreach ($rs as $key => $value) {
			$tag_ids[] = $value['tag_id'];
		}
		$tag_ids = implode(', ', $tag_ids);
		$sql = "delete from glossary where glo_tag_id in ({$tag_ids})";
		//print $sql;
		$db->query($sql);
	}

	$sql = "delete from tag where tag_pro_id = " . $pro_id;
	//print $sql;
	$db->query($sql);
	$sql = "delete from project where pro_id = " . $pro_id;
	//print $sql;
	if ($db->query($sql)) {
		echo json_encode(array("msg" => "ok"));
	} else {
		echo json_encode(array("msg" => "刪除失敗"));
	}
} elseif ($act == "chkExportTab") {
	$lv4_ids = explode(",", $_POST["lv4_ids"]);
	$export_flag = 0;
	foreach ($lv4_ids as $key => $value) {
		if (!empty($value)) {
			$sql = "select lv6.voc_name  from (
				(select voc_id, voc_pid, voc_level, voc_name from vocabulary where voc_level = 4 ) lv4 
				inner join (select voc_id, voc_pid, voc_level from vocabulary where voc_level = 5 ) lv5 on lv4.voc_id = lv5.voc_pid 
				inner join (select voc_id, voc_pid, voc_level, voc_name from vocabulary where voc_level = 6 ) lv6 on lv5.voc_id = lv6.voc_pid 
				) where lv4.voc_id = {$value}";
			$rs = $db->obj->getAll($sql);
			if (count($rs) > 1) {
				$export_flag++;
			}
		}
	}
	echo $export_flag;
} elseif ($act == "deleteGlossary") {
	$glossary_ids = implode(",", json_decode($_POST['glossary_ids'], TRUE));
	//var_export($glossary_ids);
	$sql = "delete from [glossary] where glo_id in ({$glossary_ids})";
	if ($db->query($sql)) {
		echo "OK";
	}

}
?>