﻿<?php
/**
 * PHPExcel
 *
 * Copyright (C) 2006 - 2014 PHPExcel
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2.1 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
 *
 * @category   PHPExcel
 * @package    PHPExcel
 * @copyright  Copyright (c) 2006 - 2014 PHPExcel (http://www.codeplex.com/PHPExcel)
 * @license    http://www.gnu.org/licenses/old-licenses/lgpl-2.1.txt	LGPL
 * @version    1.8.0, 2014-03-02
 */

/** Error reporting */
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
date_default_timezone_set('Asia/Taipei');


if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');
require_once 'libs/db.class.php';
require_once 'config.php';

/** Include PHPExcel */
require_once dirname(__FILE__) . '/Classes/PHPExcel.php';

// Create new PHPExcel object
$objPHPExcel = new PHPExcel();

// Set document properties
$objPHPExcel->getProperties()->setCreator("IBK")
							 ->setLastModifiedBy("IBK")
							 ->setTitle("PHPExcel Test Document")
							 ->setSubject("PHPExcel Test Document")
							 ->setDescription("Test document for PHPExcel, generated using PHP classes.")
							 ->setKeywords("office PHPExcel php")
							 ->setCategory("Test result file");							 

/* Performing SQL query */
$data = json_decode($_POST["data"], TRUE);
$lv4_ids = explode(",", $data);

//$lv4_ids = explode(",", $_GET["lv4_ids"]);

//$lv6_ids = explode(",", $_GET["lv6_ids"]);
//$tab_name = $_GET["tab_name"];
$tab_name = $_POST["tab_name"];
if (empty($tab_name)) {
	$tab_name = "專案";
}
$db = new MyDB($DSN);
$col = 0;
foreach ($lv4_ids as $key => $value) {
	if (!empty($value)) {
		$sql = "select voc_name from vocabulary where voc_id = {$value}";
		$item = $db->obj->getRow($sql);
		//${"arr".$key} = array();

		$sql = "select lv6.voc_name  from (
			(select voc_id, voc_pid, voc_level, voc_name from vocabulary where voc_level = 4 ) lv4 
			inner join (select voc_id, voc_pid, voc_level from vocabulary where voc_level = 5 ) lv5 on lv4.voc_id = lv5.voc_pid 
			inner join (select voc_id, voc_pid, voc_level, voc_name from vocabulary where voc_level = 6 ) lv6 on lv5.voc_id = lv6.voc_pid 
			) where lv4.voc_id = {$value}";
		$rs = $db->obj->getAll($sql);
		if (count($rs) > 1) {
			//$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($key, 1, $value);
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, html_entity_decode($item['voc_name']));
			$i = 2;
			
			foreach ($rs as $k => $v) {
				//$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($key, $i, $v["voc_name"]);
				$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $i, html_entity_decode($v["voc_name"]));
				$i++;
			}
			$col++;			
		}

	}                     
}
$lastColumn = $objPHPExcel->getActiveSheet()->getHighestDataColumn();
$lastColumn++;
for ($column = 'A'; $column != $lastColumn; $column++) {
	$objPHPExcel->getActiveSheet()
	    ->getStyle($column . '1')
	    ->getFill()
	    ->setFillType(PHPExcel_Style_Fill::FILL_SOLID)
	    ->getStartColor()
	    ->setARGB('FFCCCCCC');
	$objPHPExcel->getActiveSheet()->getStyle($column . '1')->getAlignment()
    ->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);    	
}
/*
foreach ($lv6_ids as $key => $value) {
	if (!empty($value)) {
		$data = explode("_", $value);
		foreach ($lv4_ids as $k => $v) {
			if (!empty($v)) {
				if ($data[0] == $v) {
					${"arr".$k}[] = $data[1];
				}
			}
		}		
	}                     
}


foreach ($lv4_ids as $key => $value) {
	if (!empty($value)) {
		$i = 2;
		foreach (${"arr".$key} as $k => $v) {
			$objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($key, $i, $v);
			$i++;
		}
	}                     
}
*/
$timestamp = date("YmdHis");
$filename = $tab_name;
$filename .= "_統計詞_" . $timestamp . ".xlsx";
//$filename = iconv('UTF-8','Big5',$filename);
// Add some data
/*
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'Hello')
            ->setCellValue('B2', 'world!')
            ->setCellValue('C1', 'Hello')
            ->setCellValue('D2', 'world!');
*/
// Miscellaneous glyphs, UTF-8
/*
$objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A4', 'Miscellaneous glyphs')
            ->setCellValue('A5', 'éàèùâêîôûëïüÿäöüç');
*/
// Rename worksheet
$objPHPExcel->getActiveSheet()->setTitle($tab_name);


// Set active sheet index to the first sheet, so Excel opens this as the first sheet
$objPHPExcel->setActiveSheetIndex(0);

ob_end_clean();
// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=utf-8');
header('Content-Disposition: attachment;filename="' . $filename . '"');
header('Cache-Control: max-age=0');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
exit;
