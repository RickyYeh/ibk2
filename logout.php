<?php
//unset($_SESSION['login_status']);
//unset($_SESSION['usr_name']);
//unset($_SESSION['usr_account']);
session_unset();
session_destroy();
session_write_close();
setcookie(session_name(),'',0,'/');
session_regenerate_id(true);
$_SESSION['login_status'] = "";
$_SESSION['usr_name'] = "";
$_SESSION['usr_account'] = "";

header("Location: login.php");
?>