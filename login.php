﻿<?php
require_once 'config.php';
?>
<!--[if IE ]><![endif]-->
<!doctype html>
<!--[if IE 8 ]> <html class="no-js lt-ie9 ie8" lang="zh-TW"> <![endif]-->
<!--[if IE 9 ]> <html class="no-js lt-ie10 ie9" lang="zh-TW"> <![endif]-->
<!--[if (gte IE 10)|!(IE)]><!-->
<html lang="zh-TW">
<!--<![endif]-->

<head>
	<title><?php echo TITLE?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/bootstrap-theme.css">
	<link rel="stylesheet" href="css/main.css?1511574384">
	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="js/bootstrap.js"></script>
	<script src="js/main.js?1540876269"></script>
	<script type="text/javascript" src="js/jquery.validate.js"></script>
	<script type="text/javascript" src="js/messages_zh_TW.js"></script>
	<script type="text/javascript" src="js/additional-methods.js"></script>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" />
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css" />	
</head>

<body>
	<div class="container">
	    <div class="row">
	        <div class="col-md-offset-4 col-md-4">
	            <div class="form-login">
	            <h4 class="login-title"><?php echo TITLE?></h4>
	            <form class="login">
		            <input type="text" id="userAccount" name="userAccount" class="form-control input-sm chat-input" placeholder="帳號" />
		            </br>
		            <input type="password" id="userPassword" name="userPassword" class="form-control input-sm chat-input" placeholder="密碼" />
		            </br>
		            <div class="wrapper">
		            <span class="group-btn">     
		                <button type="submit" class="btn btn-primary btn-md">登入 <i class="fa fa-sign-in"></i></a></button>
		            </span>
	            </form>
	            </div>
	            </div>
	        
	        </div>
	    </div>
	</div>
</body>
</html>