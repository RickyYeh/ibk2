<?php
// 设置允许其他域名访问
header('Access-Control-Allow-Origin:*');  
// 设置允许的响应类型 
header('Access-Control-Allow-Methods:POST, GET');  
// 设置允许的响应头 
header('Access-Control-Allow-Headers:x-requested-with,content-type');
ini_set("memory_limit","2048M");
	require_once '../common.php';

	$db = new MyDB($DSN);

	$pro_id = filter_var($_REQUEST['id'], FILTER_VALIDATE_INT);
	$sql = "select lv2.voc_name as industry, lv3.voc_name as feature_group, lv4.voc_name as feature, lv6.voc_name as keyword from 
	(SELECT voc_name, voc_id from [IBK].[dbo].[vocabulary] where voc_level = 2 ) lv2 left join 
	(select voc_name, voc_id, voc_pid from [IBK].[dbo].[vocabulary] where voc_level = 3) lv3 on lv2.voc_id = lv3.voc_pid  left join 
	(select voc_name, voc_id, voc_pid from [IBK].[dbo].[vocabulary] where voc_level = 4) lv4 on lv3.voc_id = lv4.voc_pid  left join 
	(select voc_name, voc_id, voc_pid from [IBK].[dbo].[vocabulary] where voc_level = 5) lv5 on lv4.voc_id = lv5.voc_pid  left join 
	(select voc_name, voc_id, voc_pid from [IBK].[dbo].[vocabulary] where voc_level = 6) lv6 on lv5.voc_id = lv6.voc_pid";
	$rs = $db->obj->getAll($sql);
//	var_export($rs);exit;
	// echo json_encode(utf8_decode('&#34;'));exit;
	// echo htmlspecialchars('&#34;', ENT_QUOTES, 'UTF-8');exit;
	// echo json_encode(htmlentities('&#34;', ENT_QUOTES|ENT_IGNORE, "UTF-8"), JSON_UNESCAPES_UNICODE);exit;
	$output = [];
//	foreach ($rs as $row) {
//		$output[] = ['industry' => str_replace('&#34;', '&#92;&#34;', $row['industry']), 'feature_group' => str_replace('&#34;', '&#92;&#34;', $row['feature_group']), 'feature' => str_replace('&#34;', '&#92;&#34;', $row['feature']), 'keyword' => str_replace('&#34;', '&#92;&#34;', $row['keyword'])];
//	}

	// $project_data = array();
	// $group = array();
		
	// foreach ($rs as $key => $value) {
	// 	$lv4_arr = array();
	// 	$lv6_arr = array();
	// 	if (!in_array(array('name' => $value['tag_name'], 'id' => $value['tag_id']), $group)) {
	// 		$tag_arr["tab_name"] = $value["tag_name"];
	// 		$tag_arr["tag_id"] = $value["tag_id"];
	// 		$group[] = array('name' => $value["tag_name"], 'id' => $value["tag_id"]);
	// 		$project_data['tag'][$tag_arr["tag_id"]] = $tag_arr;
	// 	}
		
	// 	if (!in_array(array('name' => $value['voc_name'], 'id' => $value['voc_id']), $project_data['tag'][$value['tag_id']]['lv4'])) {
	// 		$project_data['tag'][$value['tag_id']]['lv4'][$value['voc_id']]['name'] = $value['voc_name'];
	// 		$project_data['tag'][$value['tag_id']]['lv4'][$value['voc_id']]['id'] = $value['voc_id'];
	// 	}

	// 	if (!in_array(array('name' => $value['fea_name'], 'id' => $value['fea_id']), $project_data['tag'][$value['tag_id']]['lv4'][$value['voc_id']])) {
	// 		$project_data['tag'][$value['tag_id']]['lv4'][$value['voc_id']]['lv6'][] = array('name' => $value['fea_name'], 'id' => $value['fea_id']);
	// 	}			
	// }
	// $project_data['group'] = $group;
	// echo htmlspecialchars(json_encode($rs), ENT_QUOTES, 'UTF-8');

//	echo json_encode($output, JSON_UNESCAPED_UNICODE);
	$prefix = '';
	echo '[';
	foreach($rs as $row) {
		$row = ['industry' => str_replace('&#34;', '&#92;&#34;', $row['industry']), 'feature_group' => str_replace('&#34;', '&#92;&#34;', $row['feature_group']), 'feature' => str_replace('&#34;', '&#92;&#34;', $row['feature']), 'keyword' => str_replace('&#34;', '&#92;&#34;', $row['keyword'])];

		echo $prefix, json_encode($row,JSON_UNESCAPED_UNICODE);  $prefix = ',';
	}
	echo ']';
		// echo htmlspecialchars(json_encode($rs));
//var_dump(json_last_error());
?>
