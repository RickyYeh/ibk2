<?php
// 设置允许其他域名访问
header('Access-Control-Allow-Origin:*');  
// 设置允许的响应类型 
header('Access-Control-Allow-Methods:POST, GET');  
// 设置允许的响应头 
header('Access-Control-Allow-Headers:x-requested-with,content-type'); 

header("Content-type: application/json; charset=utf-8");

	require_once '../common.php';

	$db = new MyDB($DSN);

	$pro_id = filter_var($_REQUEST['id'], FILTER_VALIDATE_INT);
	if (empty($pro_id)) {
		echo json_encode(['msg' => '參數錯誤']);
	}
	$sql = "select * from [IBK].[dbo].[project] where pro_id = {$pro_id}";
	$rs = $db->obj->getRow($sql);

	$pro_type = $rs["pro_type"];
	$pro_lv2_id = $rs["pro_lv2_id"];
	if ($pro_type == 1) {
		$sql = "select b.voc_id as tag_id, b.voc_name as tag_name, c.voc_id, c.voc_pid, c.voc_name, e.voc_id as fea_id, e.voc_name as fea_name, a.voc_name as ind_name from 
	(SELECT * FROM [IBK].[dbo].vocabulary AS vocabulary_2 WHERE voc_level = 2 and voc_id = {$pro_lv2_id}) a left join 
	(SELECT * FROM [IBK].[dbo].vocabulary AS vocabulary_3 WHERE voc_level = 3) b on a.voc_id = b.voc_pid left join 
	(SELECT * FROM [IBK].[dbo].vocabulary AS vocabulary_4 WHERE voc_level = 4) c on b.voc_id = c.voc_pid left join 
	(SELECT * FROM [IBK].[dbo].vocabulary AS vocabulary_5 WHERE voc_level = 5) d on c.voc_id = d.voc_pid left join 
	(SELECT * FROM [IBK].[dbo].vocabulary AS vocabulary_6 WHERE voc_level = 6) e on d.voc_id = e.voc_pid 
	 order by tag_id, voc_id, fea_id";
	} else {
		$sql = "SELECT a.tag_id, a.tag_name, c.voc_id, c.voc_pid, c.voc_name, e.voc_id fea_id, e.voc_name AS fea_name, g.voc_name as ind_name FROM [IBK].[dbo].tag AS a LEFT OUTER JOIN
	[IBK].[dbo].[glossary] AS b ON a.tag_id = b.glo_tag_id LEFT OUTER JOIN
	[IBK].[dbo].vocabulary AS c ON b.glo_voc_id = c.voc_id LEFT OUTER JOIN 
	(SELECT voc_id, voc_pid FROM [IBK].[dbo].vocabulary WHERE voc_level = 5) AS d ON c.voc_id = d.voc_pid LEFT OUTER JOIN
	(SELECT * FROM [IBK].[dbo].vocabulary AS vocabulary_1 WHERE voc_level = 6) AS e ON d.voc_id = e.voc_pid left join 
	(SELECT * FROM [IBK].[dbo].vocabulary AS vocabulary_2 WHERE voc_level = 3) as f on c.voc_pid = f.voc_id left join 
	(SELECT * FROM [IBK].[dbo].vocabulary AS vocabulary_3 WHERE voc_level = 2) as g on f.voc_pid = g.voc_id  
	WHERE a.tag_pro_id = {$pro_id} AND b.glo_flag = 1 order by tag_id, voc_id, fea_id";
	}

	$rs = $db->obj->getAll($sql);

	if (empty($rs)) {
		echo json_encode(['msg' => '專案資料不存在']);
	} else {
		$project_data = array();
		$group = array();
			
		foreach ($rs as $key => $value) {
			$lv4_arr = array();
			$lv6_arr = array();
			if (!in_array(array('name' => $value['tag_name'], 'id' => $value['tag_id']), $group)) {
				$tag_arr["tab_name"] = $value["tag_name"];
				$tag_arr["tag_id"] = $value["tag_id"];
				$group[] = array('name' => $value["tag_name"], 'id' => $value["tag_id"]);
				$project_data['tag'][$tag_arr["tag_id"]] = $tag_arr;
			}

			if (isset($project_data['tag'][$value['tag_id']]['lv4'])) {
                if (!in_array(array('name' => $value['voc_name'], 'id' => $value['voc_id']), $project_data['tag'][$value['tag_id']]['lv4'])) {
                    if (!empty($value['voc_id'])) {
                        $project_data['tag'][$value['tag_id']]['lv4'][$value['voc_id']]['name'] = $value['voc_name'];
                        $project_data['tag'][$value['tag_id']]['lv4'][$value['voc_id']]['id'] = $value['voc_id'];
                    }
                }
                if (!in_array(array('name' => $value['fea_name'], 'id' => $value['fea_id']), $project_data['tag'][$value['tag_id']]['lv4'][$value['voc_id']])) {
                    if (!empty($value['voc_id'])) {
                        $project_data['tag'][$value['tag_id']]['lv4'][$value['voc_id']]['lv6'][] = array('name' => $value['fea_name'], 'id' => $value['fea_id']);
                    }
                }
            }


			if (!empty($value['ind_name']) && empty($project_data['ind_name'])) {
				$project_data['ind_name'] = $value['ind_name'];
			}
		}
		$project_data['group'] = $group;
		$project_data['msg'] = 'success';
		$bom = pack('H*','EFBBBF');
    	// $text = preg_replace("/^$bom/", '', json_encode($project_data));
    	echo preg_replace("/^\xef\xbb\xbf/", '', json_encode($project_data));
		// echo $text;
	}
?>
