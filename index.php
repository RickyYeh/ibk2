﻿<?php
require_once 'common.php';

if ($_SESSION['login_status'] == "") header( 'Location: login.php' );
?>

<!--[if IE ]><![endif]-->
<!doctype html>
<!--[if IE 8 ]> <html class="no-js lt-ie9 ie8" lang="zh-TW"> <![endif]-->
<!--[if IE 9 ]> <html class="no-js lt-ie10 ie9" lang="zh-TW"> <![endif]-->
<!--[if (gte IE 10)|!(IE)]><!-->
<html lang="zh-TW">
<!--<![endif]-->

<head>
	<title><?php echo TITLE?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="stylesheet" href="css/bootstrap.css">
	<link rel="stylesheet" href="css/bootstrap-theme.css">
	<link rel="stylesheet" href="css/bootstrap-select.css">
	<link rel="stylesheet" href="css/main.css?1540876269">
	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="js/bootstrap.js"></script>
	<script src="js/bootstrap-select.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.bundle.js"></script>
	<link rel="stylesheet" type="text/css" href="css/jquery.dataTables.css">
  	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.15/css/dataTables.bootstrap.min.css">
	<script type="text/javascript" charset="utf8" src="js/jquery.dataTables.js"></script>	
	<script src="js/main.js?1513089943"></script>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" />
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css" />	
</head>

<body>
	<div class="wrap">
		<div class="header_wrap">
			 <?php include ('nav.php'); ?>	
		</div>
		<div class="content_wrap">
			<div class="row">
				<div class="col-md-10 col-md-offset-1 dropdown-level">		
					<ul class="nav nav-tabs">
					  <li class="active"><a data-toggle="tab" href="#system_status">系統現況</a></li>
					  <li><a data-toggle="tab" href="#update_status">更新記錄</a></li>
					</ul>
				</div>
			</div>
			<div class="row">
			<div class="tab-content col-md-8 col-md-offset-2">
			  <div id="system_status" class="tab-pane fade in active text-center" >

						<div style="width: 500px; height: 500px; margin:20px auto 20px;">
						  <canvas id="myChart"></canvas>
						</div>
						<H3 class="pull-right" style="margin-top:-20px;">詞庫現有共<span id="total_lv6"></span>個詞</H3>
						<H4 class="text-left"><span id="lv1_stat"></span>大類</H4>
						<table class="table table-striped" id="table_system_stat">
						  <thead>
						    <tr>
						      <th class="text-center">編號</th>
						      <th class="text-center">子分類名稱</th>
						      <th class="text-center">現有詞數數</th>
						      <th class="text-center">詞總數量</th>
						    </tr>
						  </thead>
						  <tbody>
						  </tbody>
						</table>							
			  </div>

			  <div id="update_status" class="tab-pane fade">
					<table class="table table-striped" id="table_update_status" style="width:100%">
					  <thead>
					    <tr>
					      <th>編號</th>
					      <th>大類名稱</th>
					      <th>子類名稱</th>
					      <th>最近更新日期</th>
					      <th>使用者ID</th>
					    </tr>
					  </thead>
					  <tbody>

					  </tbody>
					</table>
			  </div>
			</div>

			</div>

		</div>
		<footer class="footer">

		</footer>
	</div>
</body>
</html>